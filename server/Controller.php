<?php

require_once	'Config.php';
require_once	'lib/ErrorList.php';
require_once	'lib/TextList.php';
require_once	'lib/UserList.php';
require_once	'lib/SessionList.php';
require_once	'lib/Session.php';
require_once	'lib/GameList.php';
require_once	'lib/InviteList.php';
require_once	'lib/PlayerList.php';
require_once	'lib/TurnList.php';

class Controller
{
	const	PATH_REQUEST_HANDLERS	= 'handlers';

	private $requestHandlers = array(
		'load_texts'		=> 'LoadTexts',
		//'continue_session'	=> 'ContinueSession',
		'login'				=> 'Login',
		'registration'		=> 'Registration',
		'user_registration'	=> 'Registration',
		'logout'			=> 'Logout',
		'load_user'			=> 'LoadUser',
		'users_available'	=> 'UsersAvailable',
		'invite_incoming'	=> 'InviteIncoming',
		'invite_send'		=> 'InviteSend',
		'invite_check_status'=> 'InviteCheckStatus',
		'invite_reject'		=> 'InviteReject',
		'invite_accept'		=> 'InviteAccept',
		'game_check_opponent'=> 'GameCheckOpponent',
		'game_do_turn'		=> 'GameDoTurn',
		'me_available'		=> 'MeAvailable',
		'me_not_busy'		=> 'MeNotBusy',
	);
	
	public function invoke() {
		$in = $_REQUEST;
		if ( !isset($in['action'])
			|| !($action = $in['action'])
			|| !isset($this->requestHandlers[$action])
		) {
			$out = array();
			$out['errors'] = (new ErrorList)->add(Error::REQUEST_INVALID, 'action=' .$action)
				->getErrors();
			return $out;
		}
		else {
			$handlerName = $this->requestHandlers[$action];
			require_once self::PATH_REQUEST_HANDLERS .'/' .$handlerName .'.php';
			$requestHandler = new $handlerName($in);
			try {
				$requestHandler->execute();
			} catch (Exception $e) {
			}

			$out = $requestHandler->out;
			$out['errors'] = $requestHandler->errors->getErrors();
			return $out;
		}
	}

}

