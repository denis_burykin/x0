<?php

class Config
{
	const	LANG_ID_DEFAULT		= 1;

	const	TIMER_GAME_CHECK_OPPONENT	= 1000;
	const	TIMER_USERS_AVAILABLE		= 5000;
	const	TIMER_INVITE_INCOMING		= 3000;
	const	TIMER_INVITE_CHECK_STATUS	= 5000;

	const	SESSION_COOKIES_EXPIRE	= 2592000; // 1 month
	const	SESSION_TIMEOUT 		= 3600;
	const	TURN_TIMEOUT			= 15;

	const	DB_HOST =	"127.0.0.1";
	const	DB_USER =	"root";
	const	DB_PASSWORD =	"mnb5";
	const	DB_DATABASE =	"x0";
}