<?php

require_once	$BASE_DIR .'/Config.php';

class Database
{
	private		$_connection;
	private static	$_instance;

	public static function getInstance() {
		if(!self::$_instance)
			self::$_instance = new self();
		return self::$_instance;
	}

	private function __construct() {
		$this->_connection = new mysqli(Config::DB_HOST, Config::DB_USER, 
			Config::DB_PASSWORD, Config::DB_DATABASE);
		if($this->_connection->connect_error) {
			trigger_error("Failed to connect to MySQL: "
				. $this->_connection->connect_error, E_USER_ERROR);
		$this->_connection->set_charset("utf8");
//printf("Current character set: %s\n", $this->_connection->character_set_name());
		}

	}

	private function __clone() { }

	public function getConnection() {
		return $this->_connection;
	}
}

//$x=Database::getInstance();