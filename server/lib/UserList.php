<?php

require_once	'Database.php';
require_once	'User.php';
require_once	'Session.php';

class UserList
{
	private	$users = array();

	function fetchAvailable($viewerUserId=false) {
		$db = Database::getInstance()->getConnection();
		$query = "SELECT u.id,u.name FROM users u LEFT JOIN sessions s "
				."ON u.id=s.user_id WHERE (s.status & " . Session::STATUS_FLAG_AVAILABLE
				.") AND !(s.status & " . Session::STATUS_FLAG_BUSY
				.") AND s.last_activity >= " . (time() - Config::SESSION_TIMEOUT);
		if ($viewerUserId)
			$query .= " AND u.id <> " .$viewerUserId;
		$res = $db->query($query);
		while ( ($row = $res->fetch_assoc()) ) {
			$this->users[] = new User($row['id'], $row['name']);
		}
		return $this;
	}

	function getUsers() {
		return $this->users;
	}

	function getById($id) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,name,password FROM users WHERE id='". $id."'");
		if ( !($row = $res->fetch_assoc()) )
			return false;
		return new User($row['id'], $row['name']);
	}

	function getAndCheckPasswordByName($name, $password) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,name,password FROM users WHERE name='". $name."'");
		$row = $res->fetch_assoc();
		if (!$row || $password != $row['password'])
			return false;
		return new User($row['id'], $row['name']);
	}

	function checkNameUnused($name) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id FROM users WHERE name='".$name."'");
		return !$res->num_rows;
	}

	function getIfNotTimedOutById($id) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT u.id,u.name FROM users u LEFT JOIN sessions s "
				."ON u.id=s.user_id WHERE u.id = " .$id
				." AND s.last_activity >= " . (time() - Config::SESSION_TIMEOUT)
			);
		if ( !($row = $res->fetch_assoc()) )
			return false;
		return new User($row['id'], $row['name']);
	}

}

