<?php

require_once	'Database.php';
require_once	'Player.php';
require_once	'Session.php';
require_once	'Game.php';

class PlayerList
{
	private $players = array();

	function fetchByGameId($gameId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,user_id,game_id,side FROM players WHERE game_id=". $gameId);
		while ( ($row = $res->fetch_assoc()) ) {
			$this->players[] = new Player($row['id'],$row['user_id'],$row['game_id'],$row['side']);
		}
		return $this;
	}

	function remove($id) {
		for ($i=0; $i < count($this->players); $i++)
			if ($this->players[$i]->id == $id) {
				array_splice($this->players, $i, 1);
				break;
			}
		return $this;
	}

	function getPlayers() {
		return $this->players;
	}

	function getById($id) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,user_id,game_id,side FROM players WHERE id=". $id);
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		return new Player($row['id'],$row['user_id'],$row['game_id'],$row['side']);
	}

	function getByUserId($userId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,user_id,game_id,side FROM players "
			." WHERE user_id=". $userId ." ORDER BY id DESC LIMIT 1"
			);
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		return new Player($row['id'],$row['user_id'],$row['game_id'],$row['side']);
	}

	function getGoingByUserId($userId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT p.id,p.user_id,p.game_id,p.side FROM players p "
			." LEFT JOIN games g ON p.game_id=g.id"
			." WHERE user_id=" .$userId
			." AND g.status=" .Game::STATUS_GOING
			." ORDER BY g.id DESC"
		);
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		return new Player($row['id'],$row['user_id'],$row['game_id'],$row['side']);
	}

/*
	function getAndCheckSideById($id, $side) {
		if (!$id || !is_numeric($id) || !$side || !is_numeric($side))
			return false;
		$player = $this->getById($id);
		if (!$player || $player->userId != Session::$currentSession->userId || $player->side != $side)
			return false;
		return $player;
	}
*/
}

