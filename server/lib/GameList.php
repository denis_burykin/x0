<?php

require_once	'Database.php';
require_once	'Game.php';

class GameList
{

	function getById($id) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,start_player_id,status,ctime FROM games WHERE id=". $id);
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		return new Game($row['id'],$row['start_player_id'],$row['status'],$row['ctime']);
	}

	

}

