<?php

require_once	'Database.php';
require_once	'Turn.php';

class TurnList
{
	private	$turns = array();
	
	function fetchByGameId($gameId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,game_id,player_id,cell_id,result,ctime FROM turns "
			." WHERE game_id=". $gameId
			." ORDER BY id ASC"
		);
		while ( ($row = $res->fetch_assoc()) ) {
			$this->turns[] = new Turn($row['id'],$row['game_id'],$row['player_id'],
						$row['cell_id'],$row['result'],$row['ctime'] );
		}
		return $this;
	}

	function getTurns() {
		return $this->turns;
	}
	
	function getLastTurnByGameId($gameId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,game_id,player_id,cell_id,result,ctime FROM turns "
			." WHERE game_id=". $gameId
			." ORDER BY id DESC LIMIT 1"
		);
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		$turn = new Turn($row['id'],$row['game_id'],$row['player_id'],
			$row['cell_id'],$row['result'],$row['ctime']
		);
		return $turn;
	}


}

