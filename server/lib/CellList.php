<?php

require_once	'Database.php';
require_once	'Cell.php';
require_once	'Turn.php';

class CellList
{
	private	$cells = array();

	function fetchByGameId($gameId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,game_id,col,row,content FROM cells WHERE game_id=". $gameId);
		$this->cells = array();
		while ( ($row = $res->fetch_assoc()) ) {
			$this->cells[] = new Cell($row['id'],$row['game_id'],$row['col'],$row['row'],$row['content']);
		}
		return $this;
	}

	function add(Cell $cell) {
		if (!isset($this->cells))
			$this->cells = array();
		$this->cells[] = $cell;
		return $this;
	}

	function search($id) {
		foreach ($this->cells as $cell)
			if ($cell->id == $id)
				return $cell;
		return false;
	}

	function searchByColRow($col,$row) {
		foreach ($this->cells as $cell) {
			if ($cell->col == $col && $cell->row == $row)
				return $cell;
		}
		return false;
	}

	function detectTurnResult($searchContent) {
		$lines = array(
			array( array(0,0), array(0,1), array(0,2) ),
			array( array(1,0), array(1,1), array(1,2) ),
			array( array(2,0), array(2,1), array(2,2) ),
			array( array(0,0), array(1,0), array(2,0) ),
			array( array(0,1), array(1,1), array(2,1) ),
			array( array(0,2), array(1,2), array(2,2) ),
			array( array(0,0), array(1,1), array(2,2) ),
			array( array(2,0), array(1,1), array(0,2) ),
		);

		foreach ($lines as $line) {
			$noWinningLine = false;
			for ($i=0; $i < 3; $i++) {
				$cell = $this->searchByColRow($line[$i][0], $line[$i][1]);
				if (!$cell || $cell->content != $searchContent) {
					$noWinningLine = true;
					break;
				}
			}
			if (!$noWinningLine)
				return Turn::RESULT_WIN;
		}
		if (count($this->cells) >= 9)
			return Turn::RESULT_DRAW;
		return Turn::RESULT_CONTINUE;
	}

	function getCells() {
		return $this->cells;
	}

	function getById($id) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,game_id,col,row,content FROM cells WHERE id=". $id);
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		return new Cell($row['id'],$row['game_id'],$row['col'],$row['row'],$row['content']);
	}

}

