<?php

require_once	$BASE_DIR .'/Config.php';
require_once	'Database.php';
require_once	'Session.php';

class SessionList
{
	private $sessions = array();

	function fetchTimedOut() {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,user_id,session_key,last_activity,status "
			." FROM sessions WHERE last_activity < " . (time() - Config::SESSION_TIMEOUT)
		);
		while ( ($row = $res->fetch_assoc()) ) {
			$this->sessions[] = new Session($row['id'], $row['user_id'],
				$row['session_key'], $row['last_activity'], $row['status'] );
		}
		return $this;
	}

	function getSessions() {
		return $this->sessions;
	}

	function delete() {
		$db = Database::getInstance()->getConnection();
		$ids = array();
		foreach ($this->sessions as $session)
			$ids[] = $session->id;
		$res = $db->query("DELETE FROM sessions WHERE id IN(" .join(',',$ids) .")" );
	}

	function getByKey($key) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,user_id,session_key,last_activity,status FROM sessions WHERE session_key='". $key."'");
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		$session = new Session($row['id'],$row['user_id'],$row['session_key'],$row['last_activity'],$row['status']);
		return $session;
	}

	function getByUserId($userId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,user_id,session_key,last_activity,status FROM sessions WHERE user_id=" .$userId);
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		$session = new Session($row['id'],$row['user_id'],$row['session_key'],$row['last_activity'],$row['status']);
		return $session;
	}

	function deleteByUserId($userId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("DELETE FROM sessions WHERE user_id = " . $userId );
	}


}

