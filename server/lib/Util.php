<?php

function tFont($id,$addClass=false,$extra=false) {
	$res = '<font id="' .$id .'" class="lang';
	if ($addClass)
		$res .= ' ' .$addClass;
	$res .= '"';
	if ($extra)
		$res .= ' ' .$extra;
	//$res .= '>' .TextList::get($id) .'</font>';
	$res .= '></font>';
	return $res;
//<font id="login_hdr" class="lang">Login</font>
}

function tButton($id,$addClass=false,$extra=false) {
	$res = '<input type="button" id="' .$id .'" class="btn btn-primary lang';
	if ($addClass)
		$res .= ' ' .$addClass;
	//$res .= '" value="' .TextList::get($id) .'"';
	$res .= '" value=""';
	if ($extra)
		$res .= ' ' .$extra;
	$res .= '>';
	return $res;
//<input type="button" id="login_btn" class="lang" value="Login">
}
