<?php

require_once	$BASE_DIR .'/Config.php';
require_once	'Database.php';
require_once	'Text.php';

class TextList
{
	public static	$defaultLangId = Config::LANG_ID_DEFAULT;
	public	 $currentLangId = Config::LANG_ID_DEFAULT;
	private $texts = array();

	function __construct($langId=false) {
		$this->currentLangId = $langId ?: self::$defaultLangId;
	}

	function setLanguage($langId) {
		$this->currentLangId = $langId;
	}

	function fetchAll() {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,lang_id,flags,`text` FROM texts ORDER BY id,lang_id");
		while ( ($row = $res->fetch_assoc()) ) {
			$this->texts[] = new Text($row['id'],$row['lang_id'],$row['flags'],$row['text']);
		}
		return $this;
	}

	function getTexts() {
		return $this->texts;
	}

	function getAllForClient($langId=false) {
		if (!$langId)
			$langId = $this->currentLangId;
		$texts = array();
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,`text` FROM texts WHERE lang_id=" .$langId
			." AND (flags & " .TEXT::FLAG_USED_BY_CLIENT .")"
		);
		while ( ($row = $res->fetch_assoc()) ) {
			$texts[$row['id']] = $row['text'];
		}
		return $texts;
	}
/*
	static function setDefaultLanguage($langId) {
		self::$defaultLangId = $langId;
	}
*/
	static function getTextValue($id, $langId=false) {
		if (!$langId)
			$langId = self::$defaultLangId;
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT `text` FROM texts WHERE id='". $id
			."' AND lang_id=" .$langId );
		$row = $res->fetch_assoc();
		if (!$row)
			return $id;
		else
			return $row['text'];
	}
	static function get($id, $langId=false) {
		return self::getTextValue($id, $langId);
	}

}

