<?php

require_once	'Database.php';
require_once	'TextList.php';

class Error
{
	// client side errors
	const	RESPONSE_INVALID	= 'err_response_invalid';
	const	REQUEST_FAIL		= 'err_request_fail';

	// server side errors
	const	REQUEST_INVALID		= 'err_request_invalid';
	const	PARAMETERS_INVALID	= 'err_parameters_invalid';
	const	ACTION_INEXPECTED	= 'err_action_inexpected';
	const	INTERNAL_ERROR		= 'err_internal_error';

	// user errors
	const	AUTH_REQUIRED		= 'err_auth_required';
	const	SESSION_TIMEOUT		= 'err_session_timeout';
	const	AUTH_FAILED			= 'err_auth_failed';
	const	PASSWORD_INVALID	= 'err_password_invalid';
	const	PASSWORDS_MISMATCH	= 'err_passwords_mismatch';
	const	USERNAME_INVALID	= 'err_username_invalid';
	const	USERNAME_IN_USE		= 'err_username_in_use';

	const	INVITE_DST_USER_OFFLINE	= 'err_invite_dst_user_offline';
	const	INVITE_USER_INAVAILABLE = 'err_invite_user_inavailable';
	const	INVITE_SRC_USER_LOGGED_OUT= 'err_invite_src_user_logged_out';
	const	INVITE_SENDER_CANCELLED	= 'err_invite_sender_cancelled';


	public	$id;
	public	$message;
	public	$text;

	function __construct ($id, $message='') {
		$this->id = $id;
		$this->message = $message;
		$this->text = TextList::getTextValue($id);
	}
	
}

