<?php

require_once	'Database.php';
require_once	'Lang.php';

class LangList
{
	private $langs = array();

	function fetchAll() {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,code,name FROM langs ORDER BY id");
		while ( ($row = $res->fetch_assoc()) ) {
			$this->langs[] = new Lang($row['id'],$row['code'],$row['name']);
		}
		return $this;
	}

	function getLanguages() {
		return $this->langs;
	}

	function getById($id) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,code,name FROM langs WHERE id='". $id ."'");
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		return new Lang($row['id'],$row['code'],$row['name']);
	}
	
}

