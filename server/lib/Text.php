<?php

require_once	'Database.php';

class Text
{
	const	FLAG_USED_BY_CLIENT	= 0x01;

	public	$id;
	public	$langId;
	public	$flags;
	public	$text;

	function __construct ($id, $langId, $flags, $text) {
		$this->id = $id;
		$this->langId = $langId;
		$this->flags = $flags;
		$this->text = $text;
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('"
				.$this->id . "'," .$this->langId ."," .$this->flags
				.",'" .$this->text ."')"
			);
		} else {
			$db->query("UPDATE texts SET flags=". $this->flags . ",`text`='"
				.$this->text . "' WHERE id='" . $this->id ."' AND lang_id=" .$this->langId
			);
		}
	}
	
}

