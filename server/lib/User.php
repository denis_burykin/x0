<?php

require_once	'Database.php';

class User
{
	public	$id;
	public	$name;
	public	$password;

	function __construct ($id, $name, $password='') {
		$this->id = $id;
		$this->name = $name;
		$this->password = $password;
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO users (name,password) VALUES ('" .$this->name ."','" .$this->password. "')");
			$this->id = $db->insert_id;
		} else {
			$db->query("UPDATE users SET name='". $this->name . "',password='"
				.$this->password . "' WHERE id=" . $this->id
			);
		}
	}

	static function checkNameValid($name) {
		return preg_match('/^[a-zA-Z][a-zA-Z0-9_.-]{0,31}$/',$name);
	}	
	static function checkPasswordValid($password) {
		return preg_match('/^[a-zA-Z0-9_.-]{1,32}$/',$password);
	}	
}

