<?php

require_once	'Database.php';

class Game
{
	const	STATUS_GOING	= 1;
	const	STATUS_OVER		= 2;

	public	$id;
	public	$status;
	public	$startPlayerId;
	public $cTime;

	public	$turns = array();
	public	$cells = array();

	function __construct ($id, $startPlayerId, $status=self::STATUS_GOING, $cTime=0) {
		$this->id = $id;
		$this->status = $status;
		$this->startPlayerId = $startPlayerId;
		$this->cTime = $cTime ?: time();
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO games (start_player_id,status,ctime) VALUES ("
			.$this->startPlayerId ."," .$this->status ."," .$this->cTime .")" 
			);
			$this->id = $db->insert_id;
		} else {
			$db->query("UPDATE games SET start_player_id=" . $this->startPlayerId
				.",status=". $this->status .",ctime=" .$this->cTime ." WHERE id=" .$this->id);
		}
	}

	function isStatusOver() {
		return $this->status == self::STATUS_OVER;
	}
	function setStatusOver() {
		$this->status = self::STATUS_OVER;
		return $this;
	}

	function timedOut() {
		if (!$this->turns || !count($this->turns))
			return $this->cTime < time() - Config::TURN_TIMEOUT;
		else
			return $this->turns[ count($this->turns) - 1 ]->cTime < time() - Config::TURN_TIMEOUT;
	}
	
	function load() {
		//$playerList = (new PlayerList)->fetchByGameId($this->id);
		$this->turns = (new TurnList)->fetchByGameId($this->id)->getTurns();
		$this->cells = (new CellList)->fetchByGameId($this->id)->getCells();
		return $this;
	}

}

