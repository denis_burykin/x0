<?php

require_once	'Database.php';

class Cell
{
	const	CONTENT_NONE	= 0;
	const	CONTENT_X	= 1;
	const	CONTENT_0	= 2;

	public	$id;
	public	$gameId;
	public	$col;
	public	$row;
	public	$content;

	function __construct ($id, $gameId, $col, $row, $content) {
		$this->id = $id;
		$this->gameId = $gameId;
		$this->col = $col;
		$this->row = $row;
		$this->content = $content;
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO cells (game_id,col,row,content) VALUES ("
				.$this->gameId ."," .$this->col ."," .$this->row
				."," .$this->content . ")"
			);
			$this->id = $db->insert_id;
		} else {
			$db->query("UPDATE cells SET game_id=". $this->gameId .",col="
				.$this->col .",row=" .$this->row .",content=" .$this->content
				." WHERE id=" . $this->id
			);
		}
		return $this;
	}

}

