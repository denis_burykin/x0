<?php
require_once 'Error.php';

class ErrorList
{
	private $errors = array();

	function add($id, $message='') {
		$this->errors[] = new Error($id, $message);
		return $this;
	}

	function count() {
		return count($this->errors);
	}

	function getErrors() {
		return $this->errors;
	}

}
