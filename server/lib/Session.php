<?php

require_once	$BASE_DIR .'/Config.php';
require_once	'Database.php';

class Session
{
	const	STATUS_FLAG_AVAILABLE	= 0x01; // пользователь доступен
	const	STATUS_FLAG_BUSY	= 0x02; // пользователь занят
	const	STATUS_FLAG_AVAILABLE_REPLAY= 0x04; // доступен для предложения переиграть
//	const	STATUS_	= ;

	public	$id;
	public	$userId;
	public	$sessionKey;
	public	$lastActivity;
	public	$status;

	function __construct ($id, $userId, $sessionKey, $lastActivity, $status=self::STATUS_FLAG_AVAILABLE) {
		$this->id = $id;
		$this->userId = $userId;
		$this->sessionKey = $sessionKey ?: self::generateKey();
		$this->lastActivity = $lastActivity ?: time();
		$this->status = $status;
	}

	static function generateKey() {
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$key = '';
		for ($i=0; $i < 16; $i++)
			$key .= $chars[mt_rand(0,61)];
		return $key;
	}
	static function checkKeyValid($key) {
		return $key && preg_match('/^[a-zA-Z0-9]{16}$/', $key);
	}

	function generateNewKey() {
		$this->sessionKey = self::generateKey();
		return $this;
	}

	function updateLastActivity() {
		$this->lastActivity = time();
		return $this;
	}

	function timedOut() {
		return $this->lastActivity < time() - Config::SESSION_TIMEOUT;
	}

	function isAvailable() {
		return $this->status & self::STATUS_FLAG_AVAILABLE
			&& !($this->status & self::STATUS_FLAG_BUSY);
	}
	function isAvailableReplay() {
		return $this->status & self::STATUS_FLAG_AVAILABLE_REPLAY
			&& !($this->status & self::STATUS_FLAG_BUSY);
	}

	function setAvailable() {
		$this->status |= self::STATUS_FLAG_AVAILABLE;
		$this->status &= ~self::STATUS_FLAG_AVAILABLE_REPLAY;
		return $this;
	}
	function setAvailableReplay() {
		$this->status &= ~self::STATUS_FLAG_AVAILABLE;
		$this->status |= self::STATUS_FLAG_AVAILABLE_REPLAY;
		return $this;
	}

	function isBusy() {
		return $this->status & self::STATUS_FLAG_BUSY;
	}
	function setBusy() {
		$this->status |= self::STATUS_FLAG_BUSY;
		return $this;
	}
	function removeBusy() {
		$this->status &= ~self::STATUS_FLAG_BUSY;
		return $this;
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO sessions (user_id,session_key,last_activity,status) VALUES ("
				.$this->userId .",'" .$this->sessionKey."',".$this->lastActivity. "," .$this->status.")"
			);
			$this->id = $db->insert_id;
		} else {
			$db->query("UPDATE sessions SET user_id=". $this->userId .",session_key='".$this->sessionKey. "',last_activity="
				. $this->lastActivity . ",status=" .$this->status." WHERE id=".$this->id
			);
		}
		return $this;
	}
	
}

