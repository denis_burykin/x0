<?php

require_once	'Database.php';

class Invite
{
	const	STATUS_SEND		= 1; // set by sender
	const	STATUS_REJECTED		= 2; // set by receiver
	const	STATUS_ACCEPTED		= 3; // set by receiver
	const	STATUS_CANCELLED	= 4; // set by sender
	const	STATUS_DST_USER_TIMED_OUT = 5; // set by sender
	const	STATUS_USER_LOGGED_OUT	= 6; // invites deleted at login or logout

	public	$id;
	public	$userSrcId;
	public	$userDstId;
	public	$status;
	public	$userSrc = false;

	function __construct ($id, $userSrcId, $userDstId, $status=self::STATUS_SEND) {
		$this->id = $id;
		$this->userSrcId = $userSrcId;
		$this->userDstId = $userDstId;
		$this->status = $status;
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO invites (user_src_id,user_dst_id,status) VALUES ("
				.$this->userSrcId. ",".$this->userDstId ."," .$this->status .")"
			);
			$this->id = $db->insert_id;
		} else {
			$db->query("UPDATE invites SET user_src_id=". $this->userSrcId . ",user_dst_id="
				.$this->userDstId . ",status=". $this->status ." WHERE id=" . $this->id
			);
		}
	}

	function loadUserSrc() {
		$this->userSrc = (new UserList)->getById($this->userSrcId);
	}

	function delete() {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("DELETE FROM invites WHERE id=" . $this->id);
	}

	function isSend() {
		return $this->status == self::STATUS_SEND;
	}
	
	function setRejected() {
		$this->status = self::STATUS_REJECTED;
		return $this;
	}
	function isRejected() {
		return $this->status == self::STATUS_REJECTED;
	}

	function setAccepted() {
		$this->status = self::STATUS_ACCEPTED;
		return $this;
	}
	function isAccepted() {
		return $this->status == self::STATUS_ACCEPTED;
	}

	function setCancelled() {
		$this->status = self::STATUS_CANCELLED;
		return $this;
	}
	function isCancelled() {
		return $this->status == self::STATUS_CANCELLED;
	}

	function setDstUserTimedOut() {
		$this->status = self::STATUS_DST_USER_TIMED_OUT;
		return $this;
	}
	function isDstUserTimedOut() {
		return $this->status == self::STATUS_DST_USER_TIMED_OUT;
	}

}

