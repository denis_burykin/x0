<?php

class Lang
{
	public	$id;
	public	$code;
	public	$name;

	function __construct ($id, $code, $name) {
		$this->id = $id;
		$this->code = $code;
		$this->name = $name;
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO langs (code,name) VALUES ('" .$this->code ."','" .$this->name ."')" );
			$this->id = $db->insert_id;
		} else {
			$db->query("UPDATE langs SET code='" .$this->code ."',name='" .$this->name ."' WHERE id=" . $this->id);
		}
	}
	
}

