<?php

require_once	'Database.php';
require_once	'Cell.php';
require_once	'PlayerList.php';

class Player
{
	const	SIDE_X	= 1;
	const	SIDE_0	= 2;

	public	$id;
	public	$userId;
	public	$gameId;
	public	$side;

	function __construct ($id, $userId, $gameId, $side) {
		$this->id = $id;
		$this->userId = $userId;
		$this->gameId = $gameId;
		$this->side = $side;
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO players (user_id,game_id,side) VALUES (" 
				.$this->userId ."," .$this->gameId ."," .$this->side . ")"
			);
			$this->id = $db->insert_id;
		} else {
			$db->query("UPDATE players SET user_id=". $this->userId . ",game_id="
				.$this->gameId .",side=" .$this->side . " WHERE id=" . $this->id
			);
		}
	}
	
	function cellContent() {
		return $this->side == self::SIDE_X ? Cell::CONTENT_X : Cell::CONTENT_0;
	}

	function getOpponent() {
		$players = (new PlayerList)->fetchByGameId($this->gameId)
			->remove($this->id)->getPlayers();
		return $players[0];
	}

}

