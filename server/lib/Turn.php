<?php

require_once	'Database.php';
require_once	'CellList.php';

class Turn
{
	const	RESULT_CONTINUE	= 1;
	const	RESULT_WIN		= 2;
	const	RESULT_DRAW		= 3;
	const	RESULT_SURRENDER= 4;
	const	RESULT_TIMEOUT	= 5;	

	public	$id;
	public	$gameId;
	public	$playerId;
	public	$cellId;
	public	$result;
	public $cTime;

	public	$cell;

	function __construct ($id, $gameId,$playerId,$cellId, $result=RESULT_CONTINUE, $cTime=0) {
		$this->id = $id;
		$this->gameId = $gameId;
		$this->playerId = $playerId;
		$this->cellId = $cellId;
		$this->result = $result;
		$this->cTime = $cTime ?: time();
	}

	function save() {
		$db = Database::getInstance()->getConnection();
		if (!$this->id) {
			$db->query("INSERT INTO turns (game_id,player_id,cell_id,result,ctime) VALUES ("
				.$this->gameId ."," .$this->playerId .","
				.$this->cellId ."," .$this->result ."," .$this->cTime .")"
			);
			$this->id = $db->insert_id;
		} else {
			$db->query("UPDATE turns SET game_id=". $this->gameId . ",player_id="
				.$this->playerId .",cell_id=" .$this->cellId .",result="
				.$this->result . ",ctime=" .$this->cTime ." WHERE id=" . $this->id
			);
		}
		return $this;
	}

	function loadCell(CellList $cellList=NULL) {
		if (!$this->cellId)
			return $this;
		if ($cellList)
			$this->cell = $cellList->search($this->cellId);
		else
			$this->cell = (new CellList)->getById($this->cellId);
		return $this;
	}

	function timedOut() {
		return $this->cTime < time() - Config::TURN_TIMEOUT;
	}
}

