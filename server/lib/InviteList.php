<?php

require_once	$BASE_DIR .'/Config.php';
require_once	'Database.php';
require_once	'Invite.php';
require_once	'Session.php';

class InviteList
{
	private $invites = array();

	function fetchIncoming($viewerUserId=false) {
		$db = Database::getInstance()->getConnection();
		$query = "SELECT id,user_src_id,user_dst_id,status FROM invites "
			." WHERE status=" .Invite::STATUS_SEND;
		if ($viewerUserId)
			$query .= " AND user_dst_id=" .$viewerUserId;
		$res = $db->query($query);
/*		$res = $db->query("SELECT i.id,i.user_src_id,i.user_dst_id,i.status FROM invites i "
			." LEFT JOIN sessions s ON i.user_src_id = s.user_id "
			." WHERE user_dst_id=" .Session::$currentSession->userId
			." AND s.last_activity >= " . (time() - Config::SESSION_TIMEOUT)
			." AND i.status=" .Invite::STATUS_SEND
		);
*/		while ( ($row = $res->fetch_assoc()) ) {
			$this->invites[] = new Invite($row['id'],$row['user_src_id'],
				$row['user_dst_id'],$row['status']);
		}
		return $this;
	}

	function fetchSent($senderUserId) {
		$db = Database::getInstance()->getConnection();
		$query = "SELECT id,user_src_id,user_dst_id,status FROM invites "
			." WHERE status=" .Invite::STATUS_SEND
			." AND user_src_id=" .$senderUserId;
		$res = $db->query($query);
		while ( ($row = $res->fetch_assoc()) ) {
			$this->invites[] = new Invite($row['id'],$row['user_src_id'],
				$row['user_dst_id'],$row['status']);
		}
		return $this;
	}

	function loadUserSrc() {
		foreach ($this->invites as $invite)
			$invite->loadUserSrc();
		return $this;
	}

	function getInvites() {
		return $this->invites;
	}

	function getById($id) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("SELECT id,user_src_id,user_dst_id,status FROM invites "
			." WHERE id=" .$id);
		$row = $res->fetch_assoc();
		if (!$row)
			return false;
		return new Invite($row['id'],$row['user_src_id'],$row['user_dst_id'],$row['status']);
	}

	function deleteById($id) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("DELETE FROM invites WHERE id=" . $id);
	}

	function deleteByUserId($userId) {
		$db = Database::getInstance()->getConnection();
		$res = $db->query("DELETE FROM invites WHERE user_src_id=" .$userId
			." OR user_dst_id=" .$userId );
	}
}

