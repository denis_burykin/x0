<?php
require_once 'RequestHandler.php';

class Logout extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$userId = $this->currentUserId;
		(new InviteList)->deleteByUserId($userId);
		(new SessionList)->deleteByUserId($userId);
		$this->deleteCookie();
	}

}
