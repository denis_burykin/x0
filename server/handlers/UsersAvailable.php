<?php
require_once 'RequestHandler.php';

class UsersAvailable extends RequestHandler
{

	function execute() {
		$this->checkSession();

		$this->out['users'] = (new UserList)->fetchAvailable($this->currentUserId)->getUsers();
		$this->out['invites'] = (new InviteList)->fetchIncoming($this->currentUserId)
			->loadUserSrc()->getInvites();		
	}

}
