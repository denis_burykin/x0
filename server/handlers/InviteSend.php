<?php
require_once 'RequestHandler.php';

class InviteSend extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$userDstId = $this->checkParamsPositive('user_id');
		$replay = isset($this->in['replay']) ? $this->in['replay'] : 0;
		
		if (!$this->currentSession->isAvailable() && !($replay && $this->currentSession->isAvailableReplay()) ) {
			$this->errors->add(Error::ACTION_INEXPECTED, 'sender inavailable');
			return;
		}
		$userDstSession = (new SessionList)->getByUserId($userDstId);
		if (!$userDstSession || $userDstSession->timedOut() ) {
			$this->errors->add(Error::INVITE_DST_USER_OFFLINE);
			return;
		}
		if (!$userDstSession->isAvailable() && !($replay && $userDstSession->isAvailableReplay()) ) {
			$this->errors->add(Error::INVITE_USER_INAVAILABLE);
			return;
		}

		$invite = new Invite(false, $this->currentUserId, $userDstId);
		$invite->save();
		$this->currentSession->setBusy()->save();
		$userDstSession->setBusy()->save();

		$this->out['invite_id'] = $invite->id;
	}

}
