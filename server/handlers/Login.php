<?php
require_once 'RequestHandler.php';

class Login extends RequestHandler
{

	function execute() {
		$sessionKey = $this->checkSessionKeyValid();
		
		list($name, $password) = $this->checkParams(array('name','password'));
		
		if (!User::checkNameValid($name))
			$this->errors->add(Error::USERNAME_INVALID);
		if (!User::checkPasswordValid($password))
			$this->errors->add(Error::PASSWORD_INVALID);
		if ($this->errors->count())
			return;

		$user = (new UserList)->getAndCheckPasswordByName($name, $password);
		if (!$user) {
			$this->errors->add(Error::AUTH_FAILED);
			return;
		}

		(new InviteList)->deleteByUserId($user->id);
		(new SessionList)->deleteByUserId($user->id);

		$session = new Session(false, $user->id, $sessionKey, false);
		$session->save();
		$this->setCurrentSession($session);
	
		//$this->out['session_key'] = $session->sessionKey;
		$this->setCookie();
	}

}
