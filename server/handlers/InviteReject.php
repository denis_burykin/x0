<?php
require_once 'RequestHandler.php';

class InviteReject extends RequestHandler
{

	function execute() {
		$this->checkSession();

		$inviteId = $this->checkParamsPositive('invite_id');
		if ( !($invite = (new InviteList)->getById($inviteId)) ) {
//			//("incoming invite disappeared (user seem logged out)");
//			return;
		}

		$userId = $this->currentUserId;
		if ($invite) {
			if ($invite->userDstId != $userId) {
				$this->errors->add(Error::PARAMETERS_INVALID, 'invite->userDstId != userId');
				return;
			}
			$invite->setRejected()->save();
		}
		$this->currentSession->removeBusy()->save();
	}
}
