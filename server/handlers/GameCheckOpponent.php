<?php
require_once 'RequestHandler.php';

class GameCheckOpponent extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$playerId = $this->checkParamsPositive('player_id');
		$surrender = isset($this->in['surrender']) ? $this->in['surrender'] : 0;

		if ( !($player = (new PlayerList)->getById($playerId)) 
			|| $player->userId != $this->currentUserId
		) {
			$this->errors->add(Error::PARAMETERS_INVALID, 'bad player_id');
			return;
		}

		$game = (new GameList)->getById($player->gameId);
		if ($surrender && !$game->isStatusOver()) {
			$turn = new Turn(false, $game->id, $player->id, 0, Turn::RESULT_SURRENDER);
			$turn->save();
			$game->setStatusOver()->save();
			$this->setPlayersAvailableReplay($game->id);			
			$this->out['last_turn'] = $turn;
			$this->out['game_status'] = $game->status;
			return;
		}
		$lastTurn = (new TurnList)->getLastTurnByGameId($player->gameId);
		if ($lastTurn)
			$lastTurn->loadCell();
		if ($lastTurn && $lastTurn->playerId != $player->id) {
			// был ход оппонента
			$this->out['last_turn'] = $lastTurn;
			$this->out['game_status'] = $game->status;
			return;
		}
		else {
			// оппонент не ходил
			if ( !$lastTurn && $game->timedOut()
				|| $lastTurn && $lastTurn->timedOut()
			) {
				// таймаут оппоненту
				$players = (new PlayerList)->fetchByGameId($game->id)
						->remove($this->currentUserId)->getPlayers();
				$opponentPlayer = $players[0];
				$turn = new Turn(false, $game->id, $opponentPlayer->id, 0, Turn::RESULT_TIMEOUT);
				$turn->save();
				$game->setStatusOver()->save();
				$this->out['last_turn'] = $turn;
				$this->out['game_status'] = $game->status;
				return;
			}
			else {
				// ничего не изменилось
			}
		}

	}

	function setPlayersAvailableReplay($gameId) {
		$players = (new PlayerList)->fetchByGameId($gameId)->getPlayers();
		$sessionList = new SessionList;
		foreach ($players as $player) {
			$sessionList->getByUserId($player->userId)->removeBusy()->save();
		}
	}

}
