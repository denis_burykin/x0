<?php
require_once 'RequestHandler.php';

class LoadUser extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$this->out['user'] = (new UserList)->getById($this->currentUserId);

		$player = (new PlayerList)->getGoingByUserId($this->currentUserId);
		if ($player) {
			$game = (new GameList)->getById($player->gameId)->load();
			if (!$game->timedOut()) {
				$this->out['player'] = $player;
				$this->out['game'] = $game;
				if ( !($opponentPlayer = $player->getOpponent()) ) {
					$this->errors->add(Error::INTERNAL_ERROR, 'LoadUser: active game, no opponent');
					return;
				}
				$this->out['opponent_user'] = (new UserList)->getById($opponentPlayer->userId);
			}
			else {
				$player = null;
			}
		}
		
		// отправленное приглашение подгрузить.
		$sent_invites = (new InviteList)->fetchSent($this->currentUserId)->getInvites();
		if ($sent_invites) {
			if (count($sent_invites) > 1 || $player) {
				error_log("LoadUser: count(sent_invites)=" .count($sent_invites)
					." ,player_id=" .$player->id
				);
			}
			$this->out['sent_invites'] = $sent_invites;
			$this->out['sent_invite_dst_user'] = (new UserList)->getById($sent_invites[0]->userDstId);
		}

		$invites = (new InviteList)->fetchIncoming($this->currentUserId)->getInvites();
		if (!$player && !$invites && !$sent_invites)
			if ($this->currentSession->isBusy())
				$this->currentSession->removeBusy()->save();

		return;
	}

}
