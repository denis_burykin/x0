<?php
require_once 'RequestHandler.php';

class LoadTexts extends RequestHandler
{

	function execute() {
		$langId = $this->checkParams('lang_id');
		
		$this->out['texts'] = (new TextList)->getAllForClient($langId);
	}

}
