<?php
require_once 'RequestHandler.php';

class MeAvailable extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$this->currentSession->setAvailable()->save();
	}

}
