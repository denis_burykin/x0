<?php
require_once 'RequestHandler.php';

class InviteAccept extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$inviteId = $this->checkParamsPositive('invite_id');

		$userId = $this->currentUserId;
		if ( !($invite = (new InviteList)->getById($inviteId)) ) {
			$this->errors->add(Error::INVITE_SRC_USER_LOGGED_OUT);
			return;
		}
		if ($invite->userDstId != $userId) {
			$this->errors->add(Error::PARAMETERS_INVALID, 'invite->userDstId != userId');
			return;
		}
		if ($invite->isCancelled() || $invite->isDstUserTimedOut() ) {
			$this->errors->add(Error::INVITE_SENDER_CANCELLED);
			$invite->delete();
			return;
		}

		$game = new Game(false, 0);
		$game->save();
		$player = new Player(false, $userId, $game->id, Player::SIDE_X);
		$player->save();
		$game->startPlayerId = $player->id;
		$game->save();
		$playerOpponent = new Player(false, $invite->userSrcId, $game->id, Player::SIDE_0);
		$playerOpponent->save();
		$invite->setAccepted()->save();
		$this->currentSession->setAvailableReplay()->save();

		$this->out['game'] = $game;
		$this->out['player'] = $player;
		$this->out['opponent_user'] = (new UserList)->getById($invite->userSrcId);
	}

}
