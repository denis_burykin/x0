<?php

abstract class RequestHandler
{
	abstract function execute();

	var $in;
	var $out = array();
	var $errors;

	var $currentSession;
	var $currentUserId;

	function __construct($in) {
		$this->in = $in;
		$this->errors = new ErrorList;
	}

	function checkSessionKeyValid() {
		if ( !isset($this->in['session_key'])
			|| !($sessionKey = $this->in['session_key'])
			|| !Session::checkKeyValid($sessionKey)
		) {
			$this->errors->add(Error::AUTH_REQUIRED, "invalid session key");
			$this->deleteCookie();
			throw new Exception;
		}
		return $sessionKey;
	}

	function checkSession() {
		if ( !($sessionKey = $this->checkSessionKeyValid())
			|| !($session = (new SessionList)->getByKey($sessionKey))
		) {
			$this->errors->add(Error::AUTH_REQUIRED);
			$this->deleteCookie();
			throw new Exception;
		}
		if ($session->timedOut()) {
			$this->errors->add(Error::SESSION_TIMEOUT);
			$this->deleteCookie();
			throw new Exception;
		}
		$session->updateLastActivity()->save();
		$this->setCurrentSession($session);
	}

	function setCookie() {
		//setCookie('session_key', $this->currentSession->sessionKey, time() + Config::SESSION_COOKIES_EXPIRE);
	}
	function deleteCookie() {
		//setCookie('session_key', '', 0);
	}

	function setCurrentSession($currentSession) {
		$this->currentSession = $currentSession;
		$this->currentUserId = $currentSession->userId;
	}

	function checkParamsPositive($ids) {
		$ids = is_array($ids) ? $ids : array($ids);
		$missing = array();
		$res = array();
		foreach ($ids as $id) {
			if ( !isset($this->in[$id])
				|| !($param = $this->in[$id] + 0)
				|| ($param = (int)$param) <= 0
			)
				$missing[] = $id;
			else
				$res[] = $param;
		}
		if ($missing) {
			$this->errors->add(Error::PARAMETERS_INVALID, $id ." is not a positive integer");
			throw new Exception;
		}
		return count($res) > 1 ? $res : $res[0];
	}

	function checkParams($ids) {
		$ids = is_array($ids) ? $ids : array($ids);
		$missing = array();
		$res = array();
		foreach ($ids as $id) {
			if (!isset($this->in[$id]))
				$missing[] = $id;
			else
				$res[] = $this->in[$id];
		}
		if ($missing) {
			$this->errors->add(Error::PARAMETERS_INVALID, "missing: " .implode(',', $missing));
			throw new Exception;
		}
		return count($res) > 1 ? $res : $res[0];
	}
}
