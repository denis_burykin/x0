<?php
require_once 'RequestHandler.php';

class Registration extends RequestHandler
{

	function execute() {
		$sessionKey = $this->checkSessionKeyValid();

		list($name, $password1, $password2) = $this->checkParams(
			array('name','password1','password2')
		);
		if (!User::checkPasswordValid($password1))
			$this->errors->add(Error::PASSWORD_INVALID);
		if ($password1 != $password2)
			$this->errors->add(Error::PASSWORDS_MISMATCH);

		if (!User::checkNameValid($name))
			$this->errors->add(Error::USERNAME_INVALID);
		elseif ( !(new UserList)->checkNameUnused($name) )
			$this->errors->add(Error::USERNAME_IN_USE);
		if ($this->errors->count())
			return;

		$user = new User(false,$name,$password1);
		$user->save();
		$session = new Session(false, $user->id, $sessionKey, false);
		$session->save();
		$this->setCurrentSession($session);

		//$this->out['session_key'] = $session->sessionKey;
		$this->setCookie();
	}

}
