<?php
require_once 'RequestHandler.php';

class MeNotBusy extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$this->currentSession->removeBusy()->save();
	}

}
