<?php
require_once 'RequestHandler.php';

class InviteCheckStatus extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$inviteId = $this->checkParamsPositive('invite_id');
		$inviteCancel = isset($this->in['invite_cancel']) ? $this->in['invite_cancel'] : 0;
		$doNotSetAvail = isset($this->in['do_not_set_avail']) ? $this->in['do_not_set_avail'] : 0;
		
		if ( !($invite = (new InviteList)->getById($inviteId)) ) {
			$this->out['invite_status'] = Invite::STATUS_USER_LOGGED_OUT;
			return;
		}
		if ($invite->isCancelled() || $invite->isDstUserTimedOut() ) {
			$this->errors->add(Error::ACTION_INEXPECTED, 'invite already cancelled or timed out');
			return;
		}

		$userId = $this->currentUserId;
		if ($invite->userSrcId != $userId) {
			$this->errors->add(Error::PARAMETERS_INVALID, 'invite->userSrcId != userId');
			return;
		}
		if ($invite->isRejected()) {
			$this->out['invite_status'] = $invite->status;
			$invite->delete();
			return;
		}

		$opponentUser = (new UserList)->getIfNotTimedOutById($invite->userDstId);
		if (!$opponentUser) {
			$invite->setDstUserTimedOut()->save();
			$this->out['invite_status'] = $invite->status;
			return;
		}

		if ($invite->isAccepted()) {
			$player = (new PlayerList)->getByUserId($userId);
			if (!$player) {
				$this->errors->add(Error::INTERNAL_ERROR, 'invite accepted, no opponent');
				return;
			}
			$this->currentSession->setAvailableReplay()->save();
			$this->out['game'] = (new GameList)->getById($player->gameId)->load();
			$this->out['player'] = $player;
			$this->out['opponent_user'] = $opponentUser;
			$this->out['invite_status'] = $invite->status;
			$invite->delete();
			return;
		}

		if ($inviteCancel) {
			// Получатель еще не отработал приглашение
			$userDstSession = (new SessionList)->getByUserId($invite->userDstId);
			if ($userDstSession && $userDstSession->isBusy() ) {
				$userDstSession->removeBusy()->save();
			}
			$invite->setCancelled()->save();
			$this->currentSession->removeBusy()->save();
		}
		$this->out['invite_status'] = $invite->status;

	}

}
