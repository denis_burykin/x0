<?php
require_once 'RequestHandler.php';

class InviteIncoming extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$this->out['invites'] = (new InviteList)->fetchIncoming($this->currentUserId)
				->loadUserSrc()->getInvites();
	}

}
