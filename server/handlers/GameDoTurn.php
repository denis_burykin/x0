<?php
require_once 'RequestHandler.php';

class GameDoTurn extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$playerId = $this->checkParamsPositive('player_id');
		$surrender = isset($this->in['surrender']) ? $this->in['surrender'] : 0;
		list($col, $row) = $this->checkParams(array('col','row'));
		if ($col < 0 || $col > 2 || $row < 0 || $row > 2) {
			$this->errors->add(Error::PARAMETERS_INVALID, 'col=' .$col .',row=' .$row);
			return;
		}
		if ( !($player = (new PlayerList)->getById($playerId))
			|| $player->userId != $this->currentUserId
		) {
			$this->errors->add(Error::PARAMETERS_INVALID, 'bad player_id');
			return;
		}

		$game = (new GameList)->getById($player->gameId);
		$lastTurn = (new TurnList)->getLastTurnByGameId($game->id);
		if ($lastTurn)
			$lastTurn->loadCell();

		if ($game->isStatusOver()) {
			// противник сдался или поставил таймаут. Ход неактуален.
			if (!$lastTurn) {
				$this->errors->add(Error::INTERNAL_ERROR, 'game over, no turns');
				return;
			}
			$this->out['last_turn'] = $lastTurn;
			$this->out['game_status'] = $game->status;
			return;
		}
		if ($surrender) {
			$turn = new Turn(false, $game->id, $player->id, 0, Turn::RESULT_SURRENDER);
			$turn->save();
			$game->setStatusOver()->save();
			$this->setPlayersAvailableReplay($game->id);
			$this->out['last_turn'] = $turn;
			$this->out['game_status'] = $game->status;
			return;
		}
		if ( !$lastTurn && $player->side != Player::SIDE_X
			|| $lastTurn && $lastTurn->playerId == $player->id
		) { 
			$this->errors->add(Error::ACTION_INEXPECTED, 'not your turn');
			return;
		}

		if ( $lastTurn && $lastTurn->timedOut()
			|| !$lastTurn && $game->timedOut()
		) {
			$turn = new Turn(false, $game->id, $player->id, 0, Turn::RESULT_TIMEOUT);
			$turn->save();
			$game->setStatusOver()->save();
			$this->out['last_turn'] = $turn;
			$this->out['game_status'] = $game->status;
			return;
		}

		$cellList = (new CellList)->fetchByGameId($game->id);
		if ($cellList->searchByColRow($col,$row)) {
			$this->errors->add(Error::PARAMETERS_INVALID, 'cell is not free');
			return;
		}
		$cell = new Cell(false, $game->id, $col, $row, $player->cellContent() );
		$cell->save();
		$cellList->add($cell);
		$turnResult = $cellList->detectTurnResult($player->cellContent());
		$turn = new Turn(false, $game->id, $player->id, $cell->id, $turnResult);
		$turn->save();
		$turn->loadCell($cellList);

		if ($turnResult > Turn::RESULT_CONTINUE) {
			$game->setStatusOver()->save();
			$this->setPlayersAvailableReplay($game->id);
		}
		$this->out['last_turn'] = $turn;
		$this->out['game_status'] = $game->status;
	}

	function setPlayersAvailableReplay($gameId) {
		$players = (new PlayerList)->fetchByGameId($gameId)->getPlayers();
		$sessionList = new SessionList;
		foreach ($players as $player) {
			$sessionList->getByUserId($player->userId)->removeBusy()->save();
		}
	}

}
