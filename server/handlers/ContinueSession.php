<?php
require_once 'RequestHandler.php';

class ContinueSession extends RequestHandler
{

	function execute() {
		$this->checkSession();
		
		$this->currentSession->generateNewKey()
			->updateLastActivity()->save();
		$this->out['session_key'] = $this->currentSession->sessionKey;
		$this->setCookie();
	}

}
