<?php

$BASE_DIR = 'server';

require_once	$BASE_DIR .'/Controller.php';

$controller = new Controller();
$out = $controller->invoke();

$out['request_success'] = 1;
print json_encode($out);
