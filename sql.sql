DROP DATABASE x0;
CREATE DATABASE x0;
USE x0;

SET CHARSET utf8;

CREATE TABLE users (
	id			int auto_increment,
	name		varchar(32),
	password	varchar(32),
	primary key(id)
);

CREATE TABLE sessions (
	id				int auto_increment,
	user_id			int,
	session_key		varchar(16),
	last_activity	int,
	status			tinyint,
	primary key(id)
);

CREATE TABLE invites (
	id			int auto_increment,
	user_src_id	int,
	user_dst_id	int,
	status		tinyint,
	primary key(id)
);

CREATE TABLE games (
	id				int auto_increment,
	start_player_id	int,
	status			tinyint,
	ctime			int,
	primary key(id)
);

CREATE TABLE players (
	id			int auto_increment,
	user_id		int,
	game_id		int,
	side		tinyint,
	primary key(id)
);

CREATE TABLE cells (
	id			int auto_increment,
	game_id		int,
	col			tinyint,
	row			tinyint,
	content		tinyint,
	primary key(id)
);

CREATE TABLE turns (
	id			int auto_increment,
	game_id		int,
	player_id	int,
	cell_id		int,
	result		tinyint,
	ctime		int,
	primary key(id)
);


CREATE TABLE langs (
	id			int,
	code		varchar(2),
	name		varchar(32),
	primary key(id)
) DEFAULT CHARSET=utf8;

INSERT INTO langs (id,code,name) VALUES (1,'EN','English');
INSERT INTO langs (id,code,name) VALUES (2,'RU','Русский');

CREATE TABLE texts (
	id			varchar(32),
	lang_id		smallint,
	flags		int,
	`text`		varchar(255),
	primary key(id,lang_id)
) DEFAULT CHARSET=utf8;


INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('language_select_hdr',1,1,'Select language');

INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('login_hdr',1,1,'Login');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('login_btn',1,1,'Login');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_new_window_btn',1,1,'Register new account');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_hdr',1,1,'Register new account');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_username',1,1,'User Name:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_pass1',1,1,'Password:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_pass2',1,1,'Confirm:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_new_btn',1,1,'Register New User');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_back_btn',1,1,'Back to login');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('users_active',1,1,'Available users');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_got',1,1,'invites you to play');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_sent',1,1,'You sent invitation. Waiting for opponent');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_rejected',1,1,'User rejected');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_target_user_logged_out',1,1,'User logged out');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('return_user_list_btn',1,1,'Back to user list');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_accept_btn',1,1,'Accept');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_reject_btn',1,1,'Reject');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_cancel_btn',1,1,'Cancel');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_cancelling',1,1,'Cancelling...');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('sent_invite_user_name',1,1,'');

INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('opponent_name',1,1,'Opponent:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_your_turn',1,1,'Your turn');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_opponents_turn',1,1,'Opponents turn');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_you_win',1,1,'You win');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_opponent_win',1,1,'Opponent wins');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_draw',1,1,'Draw. Game over');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_you_surrender',1,1,'You surrender');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_opponent_surrender',1,1,'Opponent surrender');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_you_timeout',1,1,'Time over');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_opponent_timeout',1,1,'Opponents time over');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('surrender_btn',1,1,'Surrender');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_replay_btn',1,1,'Invite opponent to replay');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('ok_btn',1,1,'OK');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('logout_btn',1,1,'Logout');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('return_btn',1,1,'Return');

INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_request_fail',1,1,'Request to server fails:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_response_invalid',1,1,'Invalid server response');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_request_invalid',1,1,'Invalid request');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_parameters_invalid',1,1,'Invalid request parameters');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_action_inexpected',1,1,'Action inxepected');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_internal_error',1,1,'Internal error');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_auth_required',1,1,'Authorization required');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_session_timeout',1,1,'Session timeout');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_auth_failed',1,1,'Authentication failed. Check username and password and try again');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_password_invalid',1,1,'Invalid password');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_passwords_mismatch',1,1,'Passwords mismatch');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_username_invalid',1,1,'Invalid username');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_username_in_use',1,1,'Username already in use. Choose other username');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_invite_dst_user_offline',1,1,'Target user went offline');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_invite_user_inavailable',1,1,'User already sent someone an invite or plays a game');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_invite_src_user_logged_out',1,1,'Sender already logged out');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_invite_sender_cancelled',1,1,'Sender has cancelled invitation');




INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('language_select_hdr',2,1,'Выбор языка');

INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('login_hdr',2,1,'Вход в игру');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('login_btn',2,1,'Войти');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_new_window_btn',2,1,'Зарегистрироваться');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_hdr',2,1,'Регистрация');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_username',2,1,'Имя пользователя:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_pass1',2,1,'Пароль:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_pass2',2,1,'Подтвердите пароль:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_new_btn',2,1,'Регистрация');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_back_btn',2,1,'Вход');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('users_active',2,1,'Игроки онлайн');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_got',2,1,'приглашает в игру');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_sent',2,1,'Вы отправили приглашение. Ожидание');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_rejected',2,1,'Пользователь отказался');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_target_user_logged_out',2,1,'Пользователь вышел из системы');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('return_user_list_btn',2,1,'Вернуться к списку игроков');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_accept_btn',2,1,'Принять');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_reject_btn',2,1,'Отказаться');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_cancel_btn',2,1,'Отмена');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_cancelling',2,1,'отмена...');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('sent_invite_user_name',2,1,'');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('opponent_name',2,1,'Противник:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_your_turn',2,1,'Ваш ход');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_opponents_turn',2,1,'Ход противника');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_you_win',2,1,'Вы победили');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_opponent_win',2,1,'Противник победил');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_draw',2,1,'Ничья');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_you_surrender',2,1,'Вы сдались');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_opponent_surrender',2,1,'Оппонент сдался');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_you_timeout',2,1,'Ваше время вышло');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('game_status_opponent_timeout',2,1,'Время противника вышло');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('surrender_btn',2,1,'Сдаться');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('invite_replay_btn',2,1,'Предложить новую игру');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('ok_btn',2,1,'OK');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('logout_btn',2,1,'Выход');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('return_btn',2,1,'Назад');

INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_request_fail',2,1,'Запрос к серверу не прошел:');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_response_invalid',2,1,'Неправильный ответ сервера');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_request_invalid',2,1,'Неправильный запрос к серверу');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_parameters_invalid',2,1,'Invalid request parameters');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_action_inexpected',2,1,'Action inxepected');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_internal_error',2,1,'Internal error');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_auth_required',2,1,'Нужно ввести логин и пароль');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_session_timeout',2,1,'Таймаут');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_auth_failed',2,1,'Неправильная пара имя пользователя/пароль');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_password_invalid',2,1,'Пароль может содержать только латинские символы и цифры');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_passwords_mismatch',2,1,'Пароли не совпадают');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_username_invalid',2,1,'Неправильное мя пользователя');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_username_in_use',2,1,'Пользователь с этим именем уже зарегистрирован');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_invite_dst_user_offline',2,1,'Пользователь вышел из системы');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_invite_user_inavailable',2,1,'Пользователь уже получил от кого-то приглашение или играет');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_invite_src_user_logged_out',2,1,'Пользователь, отправивший приглашение, вышел из системы');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('err_invite_sender_cancelled',2,1,'Отправитель отменил приглашение');



/*
DROP TABLE cells;
DROP TABLE games;
DROP TABLE invites;
DROP TABLE langs;
DROP TABLE texts;
DROP TABLE players;
DROP TABLE sessions;
DROP TABLE turns;
DROP TABLE users;

INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('',1,1,'');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('',1,1,'');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('',1,1,'');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('',1,1,'');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('',1,1,'');
INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('',1,1,'');
*/
/*INSERT INTO texts (id,lang_id,flags,`text`) VALUES ('',1,1,'');

REPLACE INTO texts (id,lang_id,flags,`text`) VALUES ('user_reg_username',2,1,'ru user_reg_username');
REPLACE INTO texts (id,lang_id,flags,`text`) VALUES ('login_btn',2,1,'ru login_btn');

*/

