
$(document).ready(function(){
	Application.init();
});

var	EVENT_LOGIN		= 'EVENT_LOGIN';
var	EVENT_LOGOUT	= 'EVENT_LOGOUT';

var Application = (function(){

	function init() {
		LogoutWindow.init();
		//UsersAvailableWindow.init();

		$(document).on(EVENT_LOGOUT, function() {
			RepeativeRequest.stopAll();
			$('#navbar_user_name').text('');
			LoginWindow.display();
		});

		TextStorage.selectLanguage(<?=TextList::$defaultLangId?>, 1);
			
		result = window.location.search.match(new RegExp('^\\?([a-zA-Z0-9]+)$'));
		if (result) {
			Request.setSessionKey(result[1]);
			$(document).trigger(EVENT_LOGIN);
			loadUser();
		}
		else {
			LoginWindow.display();
		}
/*
		var sessionKey = Util.getCookie('session_key');
		if (sessionKey) {
			Request.setSessionKey(sessionKey);
			Request.doPost({ action: 'continue_session' }, function(data){
				$(document).trigger(EVENT_LOGIN);
				UsersAvailableWindow.display();
			}, function(errors){
				ErrorWindow.display(errors);
				LoginWindow.display();
			});
		}
		else {
			LoginWindow.display();
		}
*/
	}

	function loadUser() {
		Request.doPost({ action: 'load_user' }, function(data){
			
			$('#navbar_user_name').text(data.user.name);
			
			if ('game' in data) {
				GameWindow.restoreGame(data.game, data.player,
					data.opponent_user,data.last_turn,data.cells);
			}
			else if ('sent_invites' in data) {
				InviteSendWindow.display(data.sent_invites[0].id, data.sent_invite_dst_user.name, 0, function(){ UsersAvailableWindow.display(); } );
			}
			else {
				UsersAvailableWindow.display();
			}
		}, function(errors) {
			ErrorWindow.display(errors);
			LoginWindow.display();
		});
	}

	/*
	*  Приложение состоит из следующих экранных элементов:
	* - navbar
	* - окно сообщения об ошибке
	* - основные окна - только одно из них может быть активно,
	*	переключаются setCurrentWindow
	* - modals
	*/
	function setCurrentWindow(name){
		var current = $('#' + name);
		$('.gameWindow').not(current).addClass('hidden');
		current.removeClass('hidden');
	}

	/*
	* статус пользователя Session::STATUS_AVAILABLE
	* это как отсутствие приглашений или текущих игр,
	* так и состояние клиента.
	*/
	function meAvailable() {
		Request.doPost({ action: 'me_available' }, function(data){
		});
	}

	return {
		init:			init,
		setCurrentWindow:setCurrentWindow,
		meAvailable:	meAvailable
	};

})();
