
<div class="modal fade" id="inviteWindow" tabindex="-1" role="dialog" aria-labelledby="inviteSendWindowLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onClick="InviteWindow.clickReject()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="inviteWindowLabel"><?=tFont('invite_got')?></h4>
      </div>
      <div class="modal-body" style="padding-bottom: 0px">
        <font id="invite_user_name"></font> <?=tFont('invite_got')?>
<!--
        <div id="invite_target_user_logged_out_alert" class="alert alert-warning">
        <strong><?=tFont('invite_target_user_logged_out', 'text-warning')?></strong>
        </div>
-->
      </div>
      <div class="modal-footer">
		<?=tButton('invite_accept_btn','','onClick="InviteWindow.clickAccept()"')?>
		<?=tButton('invite_reject_btn','btn-default','onClick="InviteWindow.clickReject()"')?>
      </div>
    </div>
  </div>
</div>

<script type='text/javascript'>
var InviteWindow = (function(){

	var click;
	var _inviteId;
	var _userName;
	var _closeCallback;

	var checkInviteRequest = new RepeativeRequest.__construct(
		{ action: 'invite_incoming' },
		<?=Config::TIMER_INVITE_INCOMING?>,
		{ stopOnError: true, delayExecution: true },
		checkInvite,
		handleError
	);

	function display(inviteId, userName, closeCallback) {
		_inviteId = inviteId;
		_userName = userName;
		_closeCallback = closeCallback;

		click = 0;
		$('#invite_user_name').text(userName);
		$('#inviteWindow').modal({ backdrop: 'static', keyboard: false });
		checkInviteRequest.start();
	}

	function checkInvite(data) {
		if (data.invites[0] && _inviteId == data.invites[0].id) {
		} else {
			Application.meAvailable();
			closeAndBack();
		}
	}

	function handleError(errors){
		ErrorWindow.display(errors);
		closeAndBack();
	}

	function clickReject() {
		if (click) return;
		click = 1;
		checkInviteRequest.stop();
		Request.doPost({ action: 'invite_reject', invite_id: _inviteId }, function(data){
			closeAndBack();
		}, function(errors){
			ErrorWindow.display(errors);
			closeAndBack();
		});
	}

	function clickAccept() {
		if (click) return;
		click = 1;
		checkInviteRequest.stop();
		Request.doPost({ action: 'invite_accept', invite_id: _inviteId }, function(data){
			close();
			GameWindow.startGame(data.game, data.player, data.opponent_user);
		}, function(errors){
			ErrorWindow.display(errors);
			Application.meAvailable();
			closeAndBack();
		});
	}

	function close() {
		$('#inviteWindow').modal('hide');
	}

	function closeAndBack() {
		close();
		if (_closeCallback)
			_closeCallback();
	}

	return {
		display:		display,
		clickReject:	clickReject,
		clickAccept:	clickAccept
	};

})();
</script>
