
<!-- Modal -->
<div class="modal fade" id="inviteSendWindow" tabindex="-1" role="dialog" aria-labelledby="inviteSendWindowLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onClick="" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<!--        <button type="button" class="close" onClick="InviteSendWindow.clickClose()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
-->        <h4 class="modal-title" id="inviteSendWindowLabel"><?=tFont('invite_sent')?></h4>
      </div>
      <div class="modal-body" style="padding-bottom: 0px">
        <?=tFont('invite_sent')?> <font id="invite_sent_user_name"></font>
        <div id="invite_cancelling_alert" class="alert alert-warning">
        <strong><?=tFont('invite_cancelling','text-warning')?></strong>
        </div>
        <div id="invite_rejected_alert" class="alert alert-warning">
        <strong><?=tFont('invite_rejected')?></strong>
        </div>
        <div id="invite_target_user_logged_out_alert" class="alert alert-warning">
        <strong><?=tFont('invite_target_user_logged_out')?></strong>
        </div>
      </div>
      <div class="modal-footer">
		<input type="button" id="invite_cancel_btn" style="float:left" class="btn btn-primary lang" onClick="InviteSendWindow.clickInviteCancel()" value="">
<!--		<?=tButton('invite_cancel_btn','','onClick="InviteSendWindow.clickInviteCancel()"')?>
-->		<?=tButton('return_btn','info hidden','onClick="InviteSendWindow.clickReturn()"')?>
      </div>
    </div>
  </div>
</div>

<script type='text/javascript'>
var InviteSendWindow = (function(){

	var click;
	var inviteStatus;
	var checkStatusTimer;
	var _inviteId;
	var _closeCallback;

	var inviteCheckStatusRequest = new RepeativeRequest.__construct(
		{ action: 'invite_check_status' },
		<?=Config::TIMER_INVITE_CHECK_STATUS?>,
		{ stopOnError: true, delayExecution: true },
		checkStatus,
		handleError
	);

	function display(inviteId, userName, ifReplay, closeCallback) {
		_closeCallback = closeCallback;
		
		inviteCheckStatusRequest.data.invite_id = inviteId;
		inviteCheckStatusRequest.data.invite_cancel = 0;
		inviteCheckStatusRequest.data.replay = ifReplay;

		click = 0;
		inviteStatus = <?=Invite::STATUS_SEND?>;

		var hideObjs = [
			'invite_cancelling_alert','invite_rejected_alert',
			'invite_target_user_logged_out_alert','return_btn'
		];
		for (var i = 0; i < hideObjs.length; i++)
			$('#'+hideObjs[i]).addClass('hidden');
		$('#invite_cancel_btn').removeClass('hidden');
		$('#invite_sent_user_name').text(userName);
		$('#inviteSendWindow').modal({ backdrop: 'static', keyboard: false });
		inviteCheckStatusRequest.start();
	}

	function checkStatus(data) {
		inviteCheckStatusRequest.data.invite_cancel = 0;
		inviteStatus = parseInt(data.invite_status);
		switch (inviteStatus) {
			case <?=Invite::STATUS_SEND?>:
				break;
			case <?=Invite::STATUS_ACCEPTED?>:
				close();
				GameWindow.startGame(data.game, data.player, data.opponent_user);
				break;
			case <?=Invite::STATUS_CANCELLED?>:
				closeAndBack();
				break;
			case <?=Invite::STATUS_REJECTED?>:
				inviteCheckStatusRequest.stop();
				$('#invite_cancelling_alert').addClass('hidden');
				$('#invite_rejected_alert').removeClass('hidden');
				$('#return_btn').removeClass('hidden');
				$('#invite_cancel_btn').addClass('hidden');
				break;
			case <?=Invite::STATUS_DST_USER_TIMED_OUT?>:
			case <?=Invite::STATUS_USER_LOGGED_OUT?>:
				inviteCheckStatusRequest.stop();
				$('#invite_cancelling_alert').addClass('hidden');
				$('#invite_target_user_logged_out_alert').removeClass('hidden');
				$('#return_btn').removeClass('hidden');
				$('#invite_cancel_btn').addClass('hidden');
				break;
		}
	}

	function handleError(errors){
		ErrorWindow.display(errors);
		closeAndBack();
	}

	function clickInviteCancel() {
		inviteCheckStatusRequest.data.invite_cancel = 1;
		$('#invite_cancel_btn').addClass('hidden');
		$('#invite_cancelling_alert').removeClass('hidden');
	}

	function clickReturn() {
		if (click) return;
		click = 1;
		Application.meAvailable();
		closeAndBack();
	}

	function clickClose() {
		if (inviteStatus == <?=Invite::STATUS_SEND?>)
			clickInviteCancel();
		else
			clickReturn();
	}

	function close() {
		inviteCheckStatusRequest.stop();
		$('#inviteSendWindow').modal('hide');
	}

	function closeAndBack() {
		close();
		if (_closeCallback)
			_closeCallback();
	}

	return {
		display:			display,
		clickClose:			clickClose,
		clickInviteCancel:	clickInviteCancel,
		clickReturn:		clickReturn
	};

})();
</script>
