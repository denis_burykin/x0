
<div id="error_window" class="hidden">
	<div class="alert alert-danger" role="alert">
	<strong><font id='error_message'></font></strong>
	<?=tButton('ok_btn','btn-danger','onClick=ErrorWindow.clear()')?>
	</div>
</div>

<script type='text/javascript'>
var ErrorWindow = (function(){

	var displayedErrors = [];

	function display(errors){
		displayedErrors = errors;
		update();
	}
	function update() {
		e = displayedErrors;
		if (!e.length) {
			$('#error_window').addClass('hidden');
			return;
		}
// переделать - чтобы вставляло элементы в DOM
		var eTexts = [];
		for (var i in e) {
			eTexts.push(TextStorage.getText(e[i].id,e[i].text) + ' ' + e[i].message);
		}
		$('#error_message').html(eTexts.join('<br>'));
		$('#error_window').removeClass('hidden');
	}
  
	function clear(){
		$('#error_window').addClass('hidden');
		displayedErrors = [];
	}

	return {
		display:	display,
		update:		update,
		clear:		clear
	};

})();
</script>

