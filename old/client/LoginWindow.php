
<div id="login_window" class="gameWindow hidden container">
	<form id=form_login method=post class="form-signin">
	<h3 class="form-signin-heading"><?=tFont('login_hdr')?></h3>
	<input type="text" id="user_reg_username" placeholder="" name="name" class="form-control input-top lang" autofocus>
	<input type="password" id="user_reg_pass1" placeholder="" name="password" class="form-control input-bottom lang">
	<button type="button" id="login_btn" class="btn btn-primary btn-block lang" onClick="LoginWindow.clickLogin()"><?=tFont('login_btn')?></button>
	<button type="button" class="btn btn-link btn-block lang" onClick="UserRegistrationWindow.display()"><?=tFont('user_reg_new_window_btn')?></button>
	</form>
</div>

<script type="text/javascript">
var LoginWindow = (function(){

	var click;

	function display() {
		click = 0;
		Application.setCurrentWindow('login_window');
	}

	function clickLogin() {
		if (click) return;
		click = 1;
		//$('input#login_btn').addClass('disabled');
		var formData = $("#form_login").serializeArray();
		formData.push({ name: 'action', value: 'login' });
		Request.doPost(formData, function(data){
			// это нужно бы на сервере делать
			window.location.search = '?' +data.session_key;
			//$(document).trigger(EVENT_LOGIN);
			//UsersAvailableWindow.display();
		}, function(errors){
			ErrorWindow.display(errors);
			click = 0;
		});
	}

	return {
		display:	display,
		clickLogin:	clickLogin
	};

})();
</script>
