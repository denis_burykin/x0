
	<?=''/*tButton('logout_btn','hidden','onClick="LogoutWindow.clickLogout()"')*/?>
<button type="button" id="logout_btn" class="btn btn-warning lang navbar-btn hidden" onClick="LogoutWindow.clickLogout()"><?=tFont('logout_btn')?></button>

<script type="text/javascript">
var LogoutWindow = (function(){

	var click = 0;

	function clickLogout(){
		click = 1;
		Request.doPost({ action: 'logout' }, function(data){
			//setCurrentWindow('login_window');
			click = 0;
			$(document).trigger(EVENT_LOGOUT);
		}, function(errors){
			ErrorWindow.display(errors);
			click = 0;
		});
	}

	function init() {
		$(document).on(EVENT_LOGIN, function() {
			$('#logout_btn').removeClass('hidden');
		});
		$(document).on(EVENT_LOGOUT, function() {
			$('#logout_btn').addClass('hidden');
		});
	}

	return {
		init:			init,
		clickLogout:	clickLogout
	};

})();
</script>
