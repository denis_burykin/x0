
<div id="users_window" class="gameWindow hidden container">
	<h5><?=tFont('users_active')?></h5>
	<div id="users_list">
	</div>
</div>

<script language="javascript">
var UsersAvailableWindow = (function(){

	var click;

	var usersRequest = new RepeativeRequest.__construct(
		{ action: 'users_available' },
		<?=Config::TIMER_USERS_AVAILABLE?>,
		{},
		displayUsers,
		displayError
	);

	function display() {
		click = 0;
		Application.setCurrentWindow('users_window');
		usersRequest.start();
	}

	function displayUsers(data) {

		$('#users_list').html('');
		if (data.invites[0]) {
			usersRequest.stop();
			InviteWindow.display(data.invites[0].id, data.invites[0].userSrc.name, function(){ display(); });
			return;
		}
		// Переделать. Вариант - использовать Underscore.js
		var content = '';
		$.each(data.users, function(index, user){
			content += '<div ';//id=' + user.id;
			content += ' onClick=UsersAvailableWindow.clickInviteSend(' +user.id +',"' +user.name +'"' +')>';
			content += user.name;
			content += '</div>\n';
		});
		$('#users_list').html(content);
		/*
		* Было бы грамотно полученных пользователей сохранить
		* (в виде объектов) в хранилище пользователей.
		*/
	}

	function displayError(errors) {
			ErrorWindow.display(errors);
	}

	function clickInviteSend(userId, userName) {
		if (click) return;
		click = 1;
		usersRequest.stop();
		inviteSend(userId);
	}

	function inviteSend(userId, userName) {
		Request.doPost({ action: 'invite_send', user_id: userId }, function(data){
			InviteSendWindow.display(data.invite_id, userName, 0, function(){ display(); } );
		},function(errors){
			ErrorWindow.display(errors);
			display();
		});
	}

	return {
		//init:			init,
		display:		display,
		clickInviteSend:clickInviteSend
	};

})();
</script>
