
var RepeativeRequest = (function(){

	/*
	* Нужно бы для списка request'ов отдельный класс
	*/
	var id = 1;
	var requests = {};

	function stopAll() {
		for (var i in requests) {
			requests[i].stop();
		}
	}

	var __construct = function(data,interval,options,callback,errorCallback) {
		this.data = data;
		this.interval = interval;
		this.options = options ? options : {};
		this.callback = callback;
		this.errorCallback = errorCallback;
		this.timer = false;
		this.running = false; // периодически выполняются запросы
		this.requestGoing = false; // происходит запрос к серверу
		this.id = id;
		requests[id++] = this;
	}

	__construct.prototype.requestCallback = function(data) {
		this.requestGoing = false;
		if (!this.running) return;
		this.runRequest();
		if (this.callback)
			this.callback(data);
	}
	__construct.prototype.requestErrorCallback = function(errors) {
		this.requestGoing = false;
		if (!this.running) return;
		if ('stopOnError' in this.options) {
			this.stop();
			return;
		}
		if (this.errorCallback)
			this.errorCallback(errors);
	}
	__construct.prototype.doRequest = function() {
		//alert('doRequest: '+this.formData);
		this.requestGoing = true;
		Request.doPost(this.data,
			$.proxy(this.requestCallback, this),
			$.proxy(this.requestErrorCallback, this)
		);
	}
	__construct.prototype.runRequest = function() {
		this.timer = setTimeout( $.proxy(this.doRequest, this), this.interval);
	}

	__construct.prototype.start = function() {
		this.running = true;
		if ( !('delayExecution' in this.options))
			this.doRequest();
		else
			this.runRequest();
	}
	__construct.prototype.stop = function() {
		clearTimeout(this.timer);
		this.running = false;
	}

	return {
		__construct:	__construct,
		stopAll:		stopAll
	};

})();
