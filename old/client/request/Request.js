
var Request = (function(){

	var	sessionKey	= '';

	function setSessionKey(key) {
		sessionKey = key;
	}

	function doPost(formData, callback, errorCallback){
		if (sessionKey)
			formData.session_key = sessionKey;
		$.post('request.php', formData, function(responseData){
			//alert(responseData);
			try {
				data = JSON.parse(responseData);
				if ( !('request_success' in data) || !data.request_success)
					throw true;
			} catch(e) {
				errors = [{
					id:	'err_response_invalid',
					text:	'Invalid response from server',
					message: ''
				}];
				if (errorCallback)
					errorCallback(errors);
				return;
			}
			if ('session_key' in data)
				setSessionKey(data.session_key);
			if (data.errors.length) {
				if (errorCallback)
					errorCallback(data.errors);
				if (data.errors[0].id == '<?=Error::SESSION_TIMEOUT?>'
						|| data.errors[0].id == '<?=Error::AUTH_REQUIRED?>')
					$(document).trigger(EVENT_LOGOUT);
			} else {
				if (callback)
					callback(data);
			}
		}).fail(function(jqXHR, textStatus, errorThrown){
//alert(errorThrown + ':' + textStatus+'!');
			errors =[{
				id:	'err_request_fail',
				text:	'Request to server failed',
				message: errorThrown + ' ' + textStatus
			}]
			if (errorCallback)
				errorCallback(errors);
		});
	}

	return {
		setSessionKey:	setSessionKey,
		doPost:			doPost
	};

})();
