
<div id="user_registration_window" class="gameWindow hidden container">
	<form id=form_user_reg_new method=post class="form-signin">
	<h3 class="form-signin-heading"><?=tFont('user_reg_hdr')?></h3>
	<input type="text" name="name" id="user_reg_username" placeholder="" class="form-control input-top lang" autofocus>
	<input type="password" id="user_reg_pass1" placeholder="" name="password1" class="form-control input-middle lang">
	<input type="password" id="user_reg_pass2" placeholder="" name="password2" class="form-control input-bottom lang">
	<button type="button" class="btn btn-primary btn-block lang" onClick="UserRegistrationWindow.clickRegistration()"><?=tFont('user_reg_new_btn')?></button>
	<button type="button" class="btn btn-link btn-block lang" onClick="LoginWindow.display()"><?=tFont('user_reg_back_btn')?></button>
	</form>
</div>

<script type="text/javascript">
var UserRegistrationWindow = (function(){

	var click;

	function display() {
		click = 0;
		Application.setCurrentWindow('user_registration_window');
	}

	function clickRegistration() {
		if (click) return;
		click = 1;
		var formData = $("#form_user_reg_new").serializeArray();
		formData.push({ name: 'action', value: 'user_registration' });
		Request.doPost(formData, function(data){
			// это нужно бы на сервере делать
			window.location.search = '?' +data.session_key;
			//$(document).trigger(EVENT_LOGIN);
			//UsersAvailableWindow.display();
		}, function(errors){
			ErrorWindow.display(errors);
			click = 0;
		});
	}

	return {
		display:			display,
		clickRegistration:	clickRegistration
	};

})();
</script>
