
<div id="game_window" class="gameWindow hidden container" style="width: 350px">

  <?=tFont('opponent_name')?><font id="opponentName"></font>
  <strong><font id="turnTimer" color=red></font></strong>
<!-- 
  <table class="gameField" border=1>
	<tr><td id="mySideCell"></td></tr>
  </table>
-->
  <table class="gameField" border=1>
	<tr>
	<td id="cell00" onClick="GameWindow.clickCell(0,0)">&nbsp;</td>
	<td id="cell10" onClick="GameWindow.clickCell(1,0)">&nbsp;</td>
	<td id="cell20" onClick="GameWindow.clickCell(2,0)">&nbsp;</td>
	</tr>
	<tr>
	<td id="cell01" onClick="GameWindow.clickCell(0,1)">&nbsp;</td>
	<td id="cell11" onClick="GameWindow.clickCell(1,1)">&nbsp;</td>
	<td id="cell21" onClick="GameWindow.clickCell(2,1)">&nbsp;</td>
	</tr>
	<tr>
	<td id="cell02" onClick="GameWindow.clickCell(0,2)">&nbsp;</td>
	<td id="cell12" onClick="GameWindow.clickCell(1,2)">&nbsp;</td>
	<td id="cell22" onClick="GameWindow.clickCell(2,2)">&nbsp;</td>
	</tr>
  </table>

	<div class="alert alert-info">
		<strong>
		<?=tFont('game_status_your_turn','gameStatus')?>
		<?=tFont('game_status_opponents_turn','gameStatus')?>
		<?=tFont('game_status_you_win','gameStatus')?>
		<?=tFont('game_status_opponent_win','gameStatus')?>
		<?=tFont('game_status_draw','gameStatus')?>
		<?=tFont('game_status_you_surrender','gameStatus')?>
		<?=tFont('game_status_opponent_surrender','gameStatus')?>
		<?=tFont('game_status_you_timeout','gameStatus')?>
		<?=tFont('game_status_opponent_timeout','gameStatus')?>
		</strong>
	</div>
	<?=tButton('surrender_btn','','onClick="GameWindow.clickSurrender()"')?>
	<div id="game_over" class="hidden">
		<?=tButton('invite_replay_btn','','onClick="GameWindow.clickInviteSend()"')?>
		<?=tButton('return_user_list_btn','','onClick="GameWindow.clickReturnToUserList()"')?>
	</div>

</div>

<script type='text/javascript'>
var GameWindow = (function(){

	var click;
	var cells;
	var lastTurn;
	var lastTurnTime;
	var game;
	var player;
	var opponentUser;
	var turnTimer;

	var opponentCheckRequest = new RepeativeRequest.__construct(
		{ action: 'game_check_opponent' },
		<?=Config::TIMER_GAME_CHECK_OPPONENT?>,
		{ delayExecution: true, stopOnError: true },
		checkOpponent,
		checkOpponentError
	);
	var checkInviteRequest = new RepeativeRequest.__construct(
		{ action: 'invite_incoming' },
		<?=Config::TIMER_INVITE_INCOMING?>,
		{ delayExecution: true, stopOnError: true },
		checkInvite,
		checkInviteError
	);	
	
	function startGame(_game, _player, _opponentUser) {
		game = _game;
		player = _player;
		opponentUser = _opponentUser;

		opponentCheckRequest.data.player_id = player.id;
		click = 0;
		clearCells();
		lastTurn = false;
		updateLastTurnTime();
		gameStatus = <?=Game::STATUS_GOING?>;
		
		updateDisplayAtStart();
		updateAfterTurn();
		Application.setCurrentWindow('game_window');
		if (!myTurn())
			opponentCheckRequest.start();
	}

	function restoreGame(_game, _player, _opponentUser, _lastTurn, _cells) {
		game = _game;
		player = _player;
		opponentUser = _opponentUser;

		opponentCheckRequest.data.player_id = player.id;
		click = 0;
		setCells(_cells);
		lastTurn = _lastTurn;
		updateLastTurnTime();
		gameStatus = _game.status;
		
		updateDisplayAtStart();
		updateAfterTurn();
		Application.setCurrentWindow('game_window');
		if (!myTurn())
			opponentCheckRequest.start();
	}

	function myTurn() {
		return lastTurn
			? lastTurn.playerId != player.id
			: game.startPlayerId == player.id;
	}

	function cellGet(col,row){
		return cells[col][row];
	}
	function cellSet(col,row,content){
		cells[col][row] = content;
		$('#cell'+col+row).hide().html(cellContentHtml(content)).show("fast");
	}
	function clearCells(){
		cells = [ [false,false,false], [false,false,false], [false,false,false] ];
		for (var i=0; i<3; i++)
			for (var j=0; j<3; j++)
				$('#cell'+i+j).html('&nbsp;');
	}
	function setCells(cells) {
		clearCells();
		for (var i=0; i < cells.length; i++)
			cellSet(cells[i].col, cells[i].row, cells[i].content);
	}

	function cellContentBySide(side) {
		return side == <?=Player::SIDE_X?> ? <?=Cell::CONTENT_X?> : <?=Cell::CONTENT_0?>;
	}
	function cellContentHtml(content) {
		return content == <?=Cell::CONTENT_X?> ? '&#215;' : '&#927;';
	}
	function opponentSide(){
		return player.side != <?=Player::SIDE_X?> ? <?=Player::SIDE_X?> : <?=Player::SIDE_0?>;
	}

	function updateLastTurnTime() {
		lastTurnTime = (new Date()).getTime() / 1000;
	}
	function updateTurnTimer() {
		remains = <?=Config::TURN_TIMEOUT?>
			- parseInt( (new Date()).getTime() / 1000 - lastTurnTime);
		if (remains < -1) {
			stopTurnTimer();
			if (myTurn())
				doTurn(0,0,1);
			else
				opponentCheckRequest.stop();
		}
		if (remains < 0)
			remains = 0;
		$('#turnTimer').text(remains);
	}

	function startTurnTimer() {
		turnTimer = setInterval(updateTurnTimer, 1000);
		updateTurnTimer();
		$('#turnTimer').show();
	}
	function stopTurnTimer() {
		clearInterval(turnTimer);
		$('#turnTimer').hide();
	}

	function updateDisplayAtStart(){
		$('#opponentName').text(opponentUser.name);
		startTurnTimer();
		$('#game_over').addClass('hidden');
		$('#invite_replay_btn').addClass('hidden');
		$('#surrender_btn').removeClass('hidden');
	}

	function updateAfterTurn(){
		if (gameStatus == <?=Game::STATUS_GOING?>) {
			if (myTurn())
				turnStatus = 'game_status_your_turn';
			else
				turnStatus = 'game_status_opponents_turn';
		}
		else {
			// game over
			stopTurnTimer();
			$('#surrender_btn').addClass('hidden');
			if (lastTurn.result == <?=Turn::RESULT_DRAW?>) {
				turnStatus = 'game_status_draw';
			}
			else if (lastTurn.result == <?=Turn::RESULT_WIN?>) {
				if (lastTurn.playerId == player.id)
					turnStatus = 'game_status_you_win';
				else
					turnStatus = 'game_status_opponent_win';
			}
			else if (lastTurn.result == <?=Turn::RESULT_SURRENDER?>) {
				if (lastTurn.playerId == player.id)
					turnStatus = 'game_status_you_surrender';
				else
					turnStatus = 'game_status_opponent_surrender';
			}
			else if (lastTurn.result == <?=Turn::RESULT_TIMEOUT?>) {
				if (lastTurn.playerId == player.id)
					turnStatus = 'game_status_you_timeout';
				else
					turnStatus = 'game_status_opponent_timeout';
			}
			else {
				//alert("lastTurn.result:" +lastTurn.result);
			}
			$('#game_over').removeClass('hidden');
			if (lastTurn.result != <?=Turn::RESULT_TIMEOUT?>) {
				if (game.startPlayerId == player.id)
					$('#invite_replay_btn').removeClass('hidden');
				else
					checkInviteRequest.start();
			}
		}
		$('.gameStatus').hide();
		$('#'+turnStatus).show();
	}

	function clickCell(col,row) {
		if (!myTurn() || cellGet(col,row) || gameStatus > <?=Game::STATUS_GOING?>)
			return;
		if (click) return;
		click = 1;
		cellSet(col, row, cellContentBySide(player.side));
		doTurn(col,row,0);
	}

	function clickSurrender() {
		if (click) return;
		click = 1;
		opponentCheckRequest.stop();
		doTurn(0,0,1);
	}

	function doTurn(col,row,surrender) {
		Request.doPost({
			action: 'game_do_turn',
			col: col,
			row: row,
			player_id: player.id,
			surrender: surrender
		}, function(data){
			lastTurn = data.last_turn;
			updateLastTurnTime();
			gameStatus = data.game_status;
			updateAfterTurn();
			if (gameStatus == <?=Game::STATUS_GOING?>) {
				opponentCheckRequest.start();
			}
			click = 0;
		}, function(errors){
			ErrorWindow.display(errors);
			click = 0;
		});
	}

	function checkOpponent(data) {
		if (!data.last_turn) {
			return;
		}
		opponentCheckRequest.stop();
		lastTurn = data.last_turn;
		updateLastTurnTime();
		gameStatus = data.game_status;
		if (data.last_turn.cell) {
			cell = data.last_turn.cell;
			cellSet(cell.col, cell.row, cellContentBySide(opponentSide()) );
		}
		updateAfterTurn();
	}

	function checkOpponentError(errors){
		ErrorWindow.display(errors);
	}

	function checkInvite(data) {
		if (data.invites[0]) {
			checkInviteRequest.stop();
			InviteWindow.display(data.invites[0].id, data.invites[0].userSrc.name, function(){ checkInviteRequest.start(); } );
			return;
		} else {
		}
	}
	function checkInviteError(errors) {
		ErrorWindow.display(errors);
	}

	function clickReturnToUserList() {
		checkInviteRequest.stop();
		Application.meAvailable();
		UsersAvailableWindow.display();
	}

	function clickInviteSend() {
		if (click) return;
		click = 1;
		inviteSend(opponentUser.id, opponentUser.name);
	}

	function inviteSend(userId, userName) {
		Request.doPost({ action: 'invite_send', user_id: userId, replay: 1 }, function(data){
			InviteSendWindow.display(data.invite_id, userName, 1, function(){ click=0; } );
		},function(errors){
			ErrorWindow.display(errors);
			click = 0;
		});
	}

	return {
		startGame:		startGame,
		restoreGame:	restoreGame,
		clickCell:		clickCell,
		clickSurrender:	clickSurrender,
		clickInviteSend:		clickInviteSend,
		clickReturnToUserList:	clickReturnToUserList
	};

})();
</script>
