<?php
/*
<div id="language_selector_window">
	<div class="form-group">
	<?=tFont('language_select_hdr')?>
	</div>
	<div class="form-group">
	<select class="form-control" onChange='TextStorage.selectLanguage(this.value)'>
<?php
	foreach ((new LangList)->fetchAll()->getLanguages() as $lang) {
		print '<option value=' .$lang->id;
		if ($lang->id == TextList::$defaultLangId)
			print ' selected';
		print '>' .$lang->name ."</option>\n";
	}
?>
	</select>
	</div>
</div>
*/
?>

<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><font id="languageSelected"><?=(new LangList)->getById(Config::LANG_ID_DEFAULT)->name?></font> <span class="caret"></span></a>
	<ul class="dropdown-menu" role="menu">
<?php
	foreach ((new LangList)->fetchAll()->getLanguages() as $lang) {
		print '<li><a onClick="clickLanguage(' .$lang->id .')">' .$lang->name;
		print "</a></li>\n";
	}
?>
	</ul>
</li>


<script type='text/javascript'>
function clickLanguage(id) {
	TextStorage.selectLanguage(id);
	$('#languageSelected').text(TextStorage.langNamesById[id]);
}
</script>
