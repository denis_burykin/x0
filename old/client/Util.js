
var Util = (function(){

	function getCookie(id) {
		if ( !(cookie = document.cookie))
			return;
		var result = cookie.match(new RegExp(id + '=([a-zA-Z0-9]+)'));
		return result ? result[1] : false;
	}

	return {
		getCookie:	getCookie
	};

})();
