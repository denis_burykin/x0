
var TextStorage = (function(){
  
	var defaultLangId	= <?=TextList::$defaultLangId?>;
	var currentLangId	= <?=TextList::$defaultLangId?>;
	var textsByLangId	= {}; // textsByLangId[langId][textId]
	var langNamesById	= {
<?php 
		foreach ((new LangList)->fetchAll()->getLanguages() as $lang)
		$res[] = "\t\t" .$lang->id .":\t'" .$lang->name ."'";
		print join(",\n", $res);
?>
	};
	
	function updateTextElements() {
		var elems = $('.lang');
		elems.filter('font').each(function(){
			$(this).text(getText(this.id));
		});
		elems.filter(':button').each(function(){
			$(this).val(getText(this.id));
		});
		elems.filter('input[placeholder]').each(function(){
			$(this).attr('placeholder',getText(this.id))
		});
		ErrorWindow.update(); // неправильно. Нужно чтобы компоненты с текстом здесь как-нибудь сами регистрировались.
							// (это если не могут вставить элементы с class=lang)
	}

	function getText(id,text) {
	if (!id)
		return text ? text : 'getText: undef';
	var t = textsByLangId;
	if (currentLangId in t && id in t[currentLangId])
		return t[currentLangId][id];
	if (text)
		return text;
	if (defaultLangId in t && id in t[defaultLangId])
		return t[defaultLangId][id];
	else
		return id;
	}
  
	function selectLanguage(langId, force) {
		if (!force && currentLangId == langId)
			return;
		if (!(langId in textsByLangId)) {
		//if (!textsByLangId.hasOwnProperty(langId)) {
			loadTexts(langId);
			return;
		}
		currentLangId = langId;
		updateTextElements();
	}
  
	function loadTexts(langId) {
		Request.doPost({ action: 'load_texts', lang_id: langId }, function(data){
			if (!('texts' in data))
				return;
			textsByLangId[langId] = {};
			for (var id in data.texts) {
				t = data.texts[id];
				if (t == '') continue;
				textsByLangId[langId][id] = t;
			}
			currentLangId = langId;
			updateTextElements();
		});
	}

	return {
		selectLanguage:	selectLanguage,
		getText:		getText,
		langNamesById:	langNamesById
	};

})();
