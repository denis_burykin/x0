<?php

$BASE_DIR = 'server';
$CLIENT_BASE_DIR = 'client';

require_once	$BASE_DIR .'/Config.php';
require_once	$BASE_DIR .'/lib/Error.php';
require_once	$BASE_DIR .'/lib/Invite.php';
require_once	$BASE_DIR .'/lib/Player.php';
require_once	$BASE_DIR .'/lib/Turn.php';
require_once	$BASE_DIR .'/lib/TextList.php';
require_once	$BASE_DIR .'/lib/LangList.php';
require_once	$BASE_DIR .'/lib/Game.php';
require_once	$BASE_DIR .'/lib/Util.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/2002/REC-xhtml1-20020801/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js'></script>
	<script src="js/bootstrap.min.js"></script>

	<link href="css/style.css" rel="stylesheet">

<script type='text/javascript'>
<?php
require_once	$CLIENT_BASE_DIR .'/Application.js';
require_once	$CLIENT_BASE_DIR .'/Util.js';
require_once	$CLIENT_BASE_DIR .'/request/Request.js';
require_once	$CLIENT_BASE_DIR .'/request/RepeativeRequest.js';
require_once	$CLIENT_BASE_DIR .'/TextStorage.js';
?>
</script>

</head>

<body>

<?php require_once	$CLIENT_BASE_DIR .'/Navbar.php'; ?>
<?php require_once	$CLIENT_BASE_DIR .'/ErrorWindow.php'; ?>
<?php require_once	$CLIENT_BASE_DIR .'/InviteSendWindow.php'; ?>
<?php require_once	$CLIENT_BASE_DIR .'/InviteWindow.php'; ?>
<?php require_once	$CLIENT_BASE_DIR .'/LoginWindow.php'; ?>
<?php require_once	$CLIENT_BASE_DIR .'/UserRegistrationWindow.php'; ?>
<?php require_once	$CLIENT_BASE_DIR .'/UsersAvailableWindow.php'; ?>
<?php require_once	$CLIENT_BASE_DIR .'/GameWindow.php'; ?>

</body>
</html>

