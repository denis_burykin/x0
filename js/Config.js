var Config = {

	Timers: {
		// проверка хода оппонента во время игры
		GAME_CHECK_OPPONENT:	1000,
		// обновление списка доступных пользователей
		USERS_AVAILABLE:		5000,
		// проверка статуса отправленного приглашения
		INVITE_CHECK_STATUS:	5000,
		// проверка на приглашение на экране игры после окончания игры
		INVITE_CHECK_INCOMING:	3000
	},
	
	Session: {
		COOKIES_EXPIRE:		2592000,
		// отсутствие запросов от приложения
		TIMEOUT:			3600
	},
	
	// Ограничение времени на ход
	TURN_TIMEOUT:		15,
	// Язык интерфейса по умолчанию
	LANG_ID_DEFAULT:	1

}; // Config