// Auto Generated!
/////////////////////
function User (){
Entity(this,'User');
}
EntityPrototype(User,'User');

function UserCollection(){
AbstractCollection(this,'User');
}
AbstractCollectionPrototype(UserCollection,'User');

function Session (){
Entity(this,'Session');
}
EntityPrototype(Session,'Session');

function SessionCollection(){
AbstractCollection(this,'Session');
}
AbstractCollectionPrototype(SessionCollection,'Session');

function Invite (){
Entity(this,'Invite');
}
EntityPrototype(Invite,'Invite');

function InviteCollection(){
AbstractCollection(this,'Invite');
}
AbstractCollectionPrototype(InviteCollection,'Invite');

function Game (){
Entity(this,'Game');
}
EntityPrototype(Game,'Game');

function GameCollection(){
AbstractCollection(this,'Game');
}
AbstractCollectionPrototype(GameCollection,'Game');

function Player (){
Entity(this,'Player');
}
EntityPrototype(Player,'Player');

function PlayerCollection(){
AbstractCollection(this,'Player');
}
AbstractCollectionPrototype(PlayerCollection,'Player');

function Cell (){
Entity(this,'Cell');
}
EntityPrototype(Cell,'Cell');

function CellCollection(){
AbstractCollection(this,'Cell');
}
AbstractCollectionPrototype(CellCollection,'Cell');

function Turn (){
Entity(this,'Turn');
}
EntityPrototype(Turn,'Turn');

function TurnCollection(){
AbstractCollection(this,'Turn');
}
AbstractCollectionPrototype(TurnCollection,'Turn');

function Language (){
Entity(this,'Language');
}
EntityPrototype(Language,'Language');

function LanguageCollection(){
AbstractCollection(this,'Language');
}
AbstractCollectionPrototype(LanguageCollection,'Language');

function Text (){
Entity(this,'Text');
}
EntityPrototype(Text,'Text');

function TextCollection(){
AbstractCollection(this,'Text');
}
AbstractCollectionPrototype(TextCollection,'Text');
