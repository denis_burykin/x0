/*
Game.prototype.deserialize = function(data){
	this.id = parseInt(data.id);
	this.startPlayerId = parseInt(data.startPlayerId);
	this.cTime = parseInt(data.cTime);
	this.cells = (new CellCollection).deserialize(data.cells);
	this.turns = (new TurnCollection);//.deserialize(data.turns);
	return this;
};
*/
Game.prototype.isOver = function(){
	return this.status > this.definition.fields.status.values.GOING;
};

Game.prototype.lastTurn = function(){
	if (!this.turns.items.length)
		return;
	return this.turns.items[this.turns.items.length - 1];
};

Game.prototype.lastTime = function(){
	var lastTurn;
	if ( (lastTurn = this.lastTurn()) )
		return lastTurn.cTime;
	else
		return this.cTime;
};

Game.prototype.proposeReplay = function(player) {
	if (!this.isOver())
		return false;
	var lastTurn = this.lastTurn();
	return lastTurn
		&& lastTurn.result != lastTurn.definition.fields.result.values.TIMEOUT
		&& this.startPlayerId == player.id;
};

Game.prototype.expectInvite = function(player) {
	if (!this.isOver())
		return false;
	var lastTurn = this.lastTurn();
	return lastTurn
		&& lastTurn.result != lastTurn.definition.fields.result.values.TIMEOUT
		&& this.startPlayerId != player.id;
};

Game.prototype.myTurn = function(player){
	if (this.isOver() || !player || !player.id)
		return;
	var lastTurn = this.lastTurn();
	return lastTurn
		? lastTurn.playerId != player.id
		: this.startPlayerId == player.id;
};

Game.prototype.expectOpponentTurn = function(player) {
	return !this.isOver() && player && player.id && !this.myTurn(player);
};

Game.prototype.addLastTurn = function(lastTurn){
	if (!lastTurn)
		return this;
	this.turns.add(lastTurn);
	if (lastTurn.cell)
		this.cells.add(lastTurn.cell);
	return this;
};

//Game.prototype.  = function(){
