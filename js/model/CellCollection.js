/*
CellCollection.prototype.deserialize = function (data){
	if (!$.isArray(data))
		return this;
	for (var i=0; i < data.length; i++) {
		this.items.push( (new Cell));//.deserialize(data[i]) );
	}
	return this;
};
*/
CellCollection.prototype.getByColRow = function (col,row){
	for (var i=0; i < this.items.length; i++)
		if (col == this.items[i].col && row == this.items[i].row)
			return this.items[i];
	return;
};

CellCollection.prototype.cellIsFree = function (col,row){
	var cell = this.getByColRow(col,row);
	return !cell || cell.content == cell.definition.fields.content.values.CONTENT_NONE;
};
