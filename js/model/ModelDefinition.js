var ModelDefinition = {

	User: {
		fields: {
			//id:	{ type: 'int' },
			name:		{ type: 'text', length: 32 },
			password:	{ type: 'text', length: 32 },
			cTime:		{ type: 'int' }
		}
	},

	Session: {
		fields: {
			userId:		{ type: 'int' },
			key:		{ type: 'text', length: 16 },
			lastActivity: { type: 'int' },
			status:		{ type: 'tinyint' }
		}
	},
	
	Invite: {
		fields: {
			userSrcId:	{ type: 'int' },
			userDstId:	{ type: 'int' },
			cTime:		{ type: 'int' },
			status:		{ type: 'tinyint', values: {
				SEND: 1, REJECTED: 2, ACCEPTED: 3, CANCELLED: 4,
				DST_USER_TIMED_OUT: 5, USER_LOGGED_OUT: 6 }
			}
		}
	},

	Game: {
		fields: {
			startPlayerId: { type: 'int' },
			cTime:		{ type: 'int' },
			status:		{ type: 'tinyint', values: { GOING: 1, OVER: 2 } }
		},/*
		ref: [
			Player.cols.gameId
		],*/
		refBy: {
			turns:		{ entity: 'Turn', field: 'gameId', order: 'asc' },
			cells:		{ entity: 'Cell', filed: 'gameId' }
		}
	},
	
	Player: {
		fields: {
			userId:		{ type: 'int' },
			gameId:		{ type: 'int' },
			side:		{ type: 'tinyint', values: { SIDE_X: 1, SIDE_0: 2 } }
		}
	},
	
	Cell: {
		fields: {
			gameId:		{ type: 'int' },
			col:		{ type: 'tinyint' },
			row:		{ type: 'tinyint' },
			content:	{ type: 'tinyint', values: {
				CONTENT_NONE: 0, CONTENT_X: 1, CONTENT_0: 2 }
			}
		}
	},
	
	Turn: {
		fields: {
			gameId:		{ type: 'int' },
			playerId:	{ type: 'int' },
			cellId:		{ type: 'int' },
			cTime:		{ type: 'int' },
			result:		{ type: 'tinyint', values: {
				CONTINUE: 1, WIN: 2, DRAW: 3, SURRENDER: 4, TIMEOUT: 5 }
			}
		}
	},
	
	Language: {
		fields: {
			name:		{ type: 'text', length: 32 }
		},
		data: [
			{ id: 1,	name: 'English' },
			{ id: 2,	name: 'Русский' }
		]
	},
	
	Text: {
		fields: {
			langId:		{ type: 'int' },
			key:		{ type: 'text', length: 32 },
			text:		{ type: 'text', length: 255 }
		}
	}

}; // ModelDefinition
