
/*
*
*	Это осталось с первой версии. Потом переделать.
*	Загружать с сервера объекты Text.
*
*/
var TextStorage = (function(){

	var textsByLangId	= {}; // textsByLangId[langId][textId]

	var defaultLangId	= Config.DEFAULT_LANG_ID;

	// TODO: Добавить свойство в User
	var currentLangId	= 1;


	function getText(id) {//,text) {
		if (!id)
			return text ? text : 'getText: undef';
		var t = textsByLangId;
		if (currentLangId in t && id in t[currentLangId])
			return t[currentLangId][id];
		//if (text)
		//	return text;
		if (defaultLangId in t && id in t[defaultLangId])
			return t[defaultLangId][id];
		else
			return id;
	}

	function selectLanguage(langId){//, force) {
		if (!langId)
			langId = defaultLangId;
		//if (!force && currentLangId == langId)
		//	return;
		if (!(langId in textsByLangId)) {
			loadTexts(langId);
			return;
		}
		currentLangId = langId;
		View.updateTextElements($('body'));
	}

	function loadTexts(langId) {
		Request.post({ action: 'load_texts', lang_id: langId }, function(data){
			if (!('texts' in data))
				return;
			textsByLangId[langId] = {};
			for (var id in data.texts) {
				t = data.texts[id];
				if (t == '') continue;
				textsByLangId[langId][id] = t;
			}
			currentLangId = langId;
			View.updateTextElements($('body'));
		});
	}

	return {
		selectLanguage:	selectLanguage,
		getText:		getText,
		//defaultLangId:	defaultLangId
	};

})();
