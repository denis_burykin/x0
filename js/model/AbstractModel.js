
/******************************************************
*
*	usage:
*
*	function User() {
*		Entity(this,'User');
*	}
*	EntityPrototype(User,'User');
*
******************************************************/
function Entity(entity,entityName) {
	entity.entityName = entityName;
}

function EntityPrototype(entityFn,entityName) {

	entityFn.prototype.definition = ModelDefinition[entityName];//ModelDefinition[entityFn.prototype.constructor.name];

	for (var k in EventDispatcher.prototype)
		entityFn.prototype[k] = EventDispatcher.prototype[k];

	entityFn.prototype.set = function(obj) {
		for (var k in obj)
			this[k] = obj[k];
		this.dispatchEvent();
	};

	/*
	*	TODO: добавить проверки
	*/
	entityFn.prototype.deserialize = function(data) {
		for (var field in this.definition.fields) {
			if (field in data) {
				var type = this.definition.fields[field].type;
				if (type == 'int' || type == 'smallint' || type == 'tinyint')
					this[field] = parseInt(data[field]);
				else
					this[field] = data[field];
			}
			else
				this[field] = null;
		}
		this['id'] = parseInt(data['id']);

		if ('refBy' in this.definition) {
			for (var refBy in this.definition.refBy) {
				if (refBy in data) {
					var type = this.definition.refBy[refBy].entity;
					this[refBy] = (new window[type +'Collection']).deserialize(data[refBy]);
				}
			}
		}

		return this;
	};

}

//var abstractEntities = {};

/******************************************************
*
*	usage:
*
*	function UserCollection() {
*		AbstractCollection(this,'User');
*	}
*	AbstractCollectionPrototype(UserCollection,'User');
*
******************************************************/
function AbstractCollection(entity,entityName) {
	entity.entityName = entityName;
	entity.items = [];
}

function AbstractCollectionPrototype(collectionFn,entityName) {

	for (var k in EventDispatcher.prototype)
		collectionFn.prototype[k] = EventDispatcher.prototype[k];

	collectionFn.prototype.set = function(items) {
		this.items = items;
		this.dispatchEvent();
	};

	collectionFn.prototype.add = function(item) {
		this.items.push(item);
		this.dispatchEvent();
	};

	/*
	*	TODO: добавить проверки
	*/
	collectionFn.prototype.deserialize = function(data) {
		if (!$.isArray(data))
			return this;
		for (var i=0; i < data.length; i++) {
			var item = (new window[this.entityName]).deserialize(data[i]);
			if (item)
				this.items.push(item);
		}
		return this;
	};

	collectionFn.prototype.getById = function(id) {
		for (var i=0; i < this.items.length; i++)
			if (id == this.items[i].id)
				return this.items[i];
		return null;
	};
/*
	collectionFn.prototype.getItems = function() {
		return this.items;
	};
*/
}

//var abstractCollections = {};

/******************************************************
*
*
******************************************************/

/*
var code = "// Auto Generated!<br>\n/////////////////////<br>\n";
for (var entity in ModelDefinition) {
	code += "function " +entity +" (){<br>\n";
	code += "\tEntity(this,'" +entity +"');<br>\n";
	code += "}<br>\n";
	code += "EntityPrototype(" +entity +",'" +entity +"');<br>\n";
	code += "<br>\n";

	code += "function " +entity +"Collection(){<br>\n";
	code += "\tAbstractCollection(this,'" +entity +"');<br>\n";
	code += "}<br>\n";
	code += "AbstractCollectionPrototype(" +entity +"Collection,'" +entity +"');<br>\n";
	code += "<br>\n";
}
document.write(code);
*/
