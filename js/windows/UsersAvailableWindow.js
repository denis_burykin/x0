
function UsersAvailableModel() {
	this.users = new UserCollection;
}

UsersAvailableModel.prototype = {
};


function UsersAvailableView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;
	
	this.onClickUser = new EventDispatcher({ autoDisable: 1 });

	displayElem.load(this.resourceLoaded,this);
}

UsersAvailableView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;

		this.elems = {
			userList:	jqElem.find('#users_list')
		};

		this.elems.userList.click( $.proxy(function(e) {
			if ( (res = e.target.id.match(/^clickUser(\d+)$/)) )
				this.onClickUser.dispatchEvent({ userId: res[1] });
			return false;
		},this) );

		this.model.users.addListener(this.update,this);

		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},

	update:	function() {
		var content = '';
		$.each(this.model.users.items, function(index, user){
			content += '<div id="clickUser' + user.id +'">';
			content += user.name;
			content += "</div>\n";			
		});
		this.elems.userList.html(content);
	},

	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}
		this.update();

		this.jqElem.removeClass('hidden');
		return this;
	},

	hide: function() {
		this.jqElem
			? this.jqElem.addClass('hidden')
			: this.displayElem.showAfterLoad = 0;
		return this;
	}
	
}


function UsersAvailableController(model, view) {

	this.model = model;
	this.view = view;

	
	view.onClickUser.addListener(this.clickUser, this);
	
	this.usersRequest = new RepeativeRequest.__construct(
		{ action: 'users_available' },
		Config.Timers.USERS_AVAILABLE,
		{},
		function(data) {
			this.model.users.set(data.users);

			if (data.invites[0]) {
				this.deactivate();
				
				AppWindow.get('InviteWindow',this.inviteCallback,this)
					.controller.setData(data.invites[0].id, data.invites[0].userSrc.name)
					.invoke();
			}
		}, this,
		function(errors) {
			this.displayErrors(errors);
		}
	);

}

UsersAvailableController.prototype = {

	clickUser: function(sender,args) {
		this.deactivate();
		Request.post({ action: 'invite_send', user_id: args.userId },function(data){

			AppWindow.get('InviteSendWindow',this.inviteCallback,this)
				.controller.setData(
					data.invite_id,
					this.model.users.getById(args.userId).name
				).invoke();

		},this,function(errors){
			this.displayErrors(errors);
			this.activate();
		});
	},

	inviteCallback: function(sender,args){
		sender.close();
		if ('inviteReject' in args) {
			this.activate();
		}
		else if ('game' in args) {
			this.callback.call(this.context,this, args);
		}
		else {
			this.activate();
			alert("UsersAvailableWindow.inviteCallback: bad args");
		}
	},

	displayErrors: function(errors) {
		var parentId = this.view.displayElem.options.id;
		AppWindow.get('ErrorWindow',{ parentId: parentId, appId: parentId })
			.controller.setData(errors).invoke();
	},

	activate: function() {
		this.view.onClickUser.enable();
		this.usersRequest.start();
		return this;
	},	
	deactivate: function() {
		this.view.onClickUser.disable();
		this.usersRequest.stop();
		return this;
	}

};

function UsersAvailableWindow(displayElem) {

	this.model = new UsersAvailableModel;
	this.view = new UsersAvailableView(this.model, displayElem);
	this.controller = new UsersAvailableController(this.model, this.view);

}

UsersAvailableWindow.prototype = {
}
