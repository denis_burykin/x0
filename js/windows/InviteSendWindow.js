
function InviteSendModel() {

	this.inviteId;
	this.userName;
	this.ifReplay;
	
	this.statusValues = ModelDefinition.Invite.fields.status.values;

	this.inviteStatus = new EventDispatcher({ value: this.statusValues.SEND });
	this.inviteCancel = new EventDispatcher({ value: 0 });
}
InviteSendModel.prototype = {
};

function InviteSendView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;

	this.controls = new View.DispatcherGroup([
		this.onClickCancel = new EventDispatcher,
		this.onClickReturn = new EventDispatcher,
		this.onClickClose = new EventDispatcher
	]);

	displayElem.load(this.resourceLoaded, this);
}

InviteSendView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;

		this.elems = {
			cancelBtn:		jqElem.find('#invite_cancel_ctrl'),
			returnBtn:		jqElem.find('#return_ctrl'),
			closeBtn:		jqElem.find('#close_ctrl'),
			cancellingAlert:jqElem.find('#invite_cancelling_alert'),
			rejectedAlert:	jqElem.find('#invite_rejected_alert'),
			loggedOutAlert:	jqElem.find('#invite_target_user_logged_out_alert')
		};
		
		this.elems.cancelBtn.click( $.proxy(function(e) {
			this.onClickCancel.dispatchEvent();
			return false;
		}, this) );

		this.elems.returnBtn.click( $.proxy(function(e) {
			this.onClickReturn.dispatchEvent();
			return false;
		}, this) );

		this.elems.closeBtn.click( $.proxy(function(e) {
			this.onClickClose.dispatchEvent();
			return false;
		}, this) );


		this.model.inviteStatus.addListener(this.update, this);

		this.model.inviteCancel.addListener( function(inviteCancel){
			if (inviteCancel.value) {
				this.elems.cancelBtn.addClass('hidden');
				this.elems.cancellingAlert.removeClass('hidden');
			}
		}, this).dispatchEvent();


		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},

	update: function() {
		var statusValues = this.model.statusValues;
		switch (this.model.inviteStatus.value) {
		
			case statusValues.SEND:
				this.elems.cancellingAlert.addClass('hidden');
				this.elems.cancelBtn.removeClass('hidden');
				this.elems.rejectedAlert.addClass('hidden');
				this.elems.loggedOutAlert.addClass('hidden');
				this.elems.returnBtn.addClass('hidden');
				break;
				
			case statusValues.ACCEPTED:
				break;
			case statusValues.CANCELLED:
				break;
				
			case statusValues.REJECTED:
				this.elems.rejectedAlert.removeClass('hidden');
				this.updateOnStatusFinished();
				break;
				
			case statusValues.STATUS_DST_USER_TIMED_OUT:
			case statusValues.STATUS_USER_LOGGED_OUT:
				this.elems.loggedOutAlert.removeClass('hidden');
				this.updateOnStatusFinished();
				break;
		}
	},
	
	updateOnStatusFinished: function() {
		this.elems.cancelBtn.addClass('hidden');
		this.elems.cancellingAlert.addClass('hidden');

		this.elems.returnBtn.removeClass('hidden');
		this.controls.enable();
	},

	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}
		this.update();
		this.jqElem.find('#invite_sent_user_name').text(this.model.userName);
		
		ModalBSManager.show(this.jqElem);
		//console.log(this);
		//this.jqElem.modal({ backdrop: 'static', keyboard: false });
	},

	hide: function() {
		this.jqElem
			? ModalBSManager.hide(this.jqElem)
			: this.displayElem.showAfterLoad = 0;
	}
};

function InviteSendController(model, view) {

	this.model = model;
	this.view = view;
	
	view.onClickCancel.addListener(this.doCancel,this);

	view.onClickReturn.addListener(this.doReturn,this);
	
	view.onClickClose.addListener(function(){
		this.model.inviteStatus.value == this.model.statusValues.SEND
			? this.doCancel()
			: this.doReturn();
	}, this);
	
	this.inviteCheckStatusRequest = new RepeativeRequest.__construct(
		{ action: 'invite_check_status' },
		Config.Timers.INVITE_CHECK_INCOMING,
		{ //stopOnError: true, 
		delayExecution: true },
		function(data) {
			var inviteStatus = parseInt(data.invite_status);
			this.model.inviteStatus.set(inviteStatus);
			var statusValues = this.model.statusValues;
			
			if (inviteStatus > statusValues.SEND)
				this.inviteCheckStatusRequest.stop();

			switch (inviteStatus) {
				case statusValues.SEND:
					break;
					
				case statusValues.ACCEPTED:
					this.view.controls.disable();
					this.callback.call(this.context,this, {
						game:		(new Game).deserialize(data.game),
						player:		(new Player).deserialize(data.player),
						opponentUser: (new User).deserialize(data.opponent_user)
					});
					break;
					
				case statusValues.CANCELLED:
					this.view.controls.disable();
					this.callback.call(this.context,this, { inviteReject: 1 });
					break;
					
				case statusValues.REJECTED:
					break;
				case statusValues.STATUS_DST_USER_TIMED_OUT:
				case statusValues.STATUS_USER_LOGGED_OUT:
					break;
			}

		},this,
		function(errors) {
			this.displayErrors(errors);
		}
	);

}

InviteSendController.prototype = {

	doCancel: function(){
		this.inviteCheckStatusRequest.data.invite_cancel = 1;
		this.model.inviteCancel.set(1);
	},
	doReturn: function(){
		this.deactivate();
		Request.post({ action: 'me_not_busy' });
		this.callback.call(this.context,this, { inviteReject: 1 });
	},

	setData: function(inviteId, userName, ifReplay) {
		this.model.inviteId = inviteId;
		this.model.userName = userName;
		this.model.ifReplay = ifReplay ? ifReplay : 0;
		return this;
	},

	displayErrors: function(errors) {
		var parentId = this.view.displayElem.options.id;
		AppWindow.get('ErrorWindow',{ parentId: 'error_window_prepend', appId: parentId })
			.controller.setData(errors).invoke();
	},
	
	activate: function() {
		this.model.inviteStatus.set(this.model.statusValues.SEND);
		this.model.inviteCancel.set(0);

		this.inviteCheckStatusRequest.data.invite_id = this.model.inviteId;
		this.inviteCheckStatusRequest.data.invite_cancel = 0;
		this.inviteCheckStatusRequest.data.replay = this.model.ifReplay;
		this.inviteCheckStatusRequest.start();
		
		this.view.controls.enable();

		return this;
	},
	deactivate: function() {
		this.view.controls.disable();
		this.inviteCheckStatusRequest.stop();
		return this;
	}
};

function InviteSendWindow(displayElem) {

	this.model = new InviteSendModel;
	this.view = new InviteSendView(this.model, displayElem);
	this.controller = new InviteSendController(this.model, this.view);

}
InviteSendWindow.prototype = {
};
