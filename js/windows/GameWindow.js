
function GameModel() {

	this.game;
	this.player;
	this.opponentUser;

	this.onChange = new EventDispatcher;

	this.timeRemains = new EventDispatcher({ value: Config.TURN_TIMEOUT });
	this.lastTurnClientTime;
	
	this.turnResultValues = ModelDefinition.Turn.fields.result.values;
	//this.playerSideValues = ModelDefinition.Player.fields.side.values;
	this.cellContentValues = ModelDefinition.Cell.fields.content.values;
}

GameModel.prototype = {

	setLastTurnClientTime: function() {
		this.lastTurnClientTime = parseInt( (new Date).getTime() / 1000 );
	},
	
};


function GameView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;

	this.gameControls = new View.DispatcherGroup([
		this.onClickGameField = new EventDispatcher,
		this.onClickSurrender = new EventDispatcher
	], { autoDisable: 0 });

	this.gameOverControls = new View.DispatcherGroup([
		this.onClickInviteReplay = new EventDispatcher,
		this.onClickUserList = new EventDispatcher
	]);
	
	displayElem.load(this.resourceLoaded, this);
}

GameView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;

		this.elems = {
			opponentName:		jqElem.find('#opponentName'),
			turnTimer:			jqElem.find('#turnTimer'),
			gameField:			jqElem.find('#gameField'),

			statusYourTurn:		jqElem.find('#game_status_your_turn'),
			statusOpponentTurn:	jqElem.find('#game_status_opponents_turn'),
			statusYouWin:		jqElem.find('#game_status_you_win'),
			statusOpponentWin:	jqElem.find('#game_status_opponent_win'),
			statusDraw:			jqElem.find('#game_status_draw'),
			statusYouSurrender:	jqElem.find('#game_status_you_surrender'),
			statusOpponentSurrender:jqElem.find('#game_status_opponent_surrender'),
			statusYouTimeout:	jqElem.find('#game_status_you_timeout'),
			statusOpponentTimeout:jqElem.find('#game_status_opponent_timeout'),

			gameOver:			jqElem.find('#game_over'),
			surrenderBtn:		jqElem.find('#surrender_ctrl'),
			inviteReplayBtn:	jqElem.find('#invite_replay_ctrl'),
			userListBtn:		jqElem.find('#return_user_list_ctrl'),
			
			turnTimer:			jqElem.find('#turnTimer')
			//:		jqElem.find('#'),
		};

		this.elems.gameField.click( $.proxy(function(e) {
			if ( (res = e.target.id.match(/^cell(\d)(\d)$/)) )
				this.onClickGameField.dispatchEvent({ col: res[1], row: res[2] });
			return false;
		}, this) );

		this.elems.surrenderBtn.click( $.proxy(function(e) {
			this.onClickSurrender.dispatchEvent();
		}, this) );

		this.elems.inviteReplayBtn.click( $.proxy(function(e) {
			this.onClickInviteReplay.dispatchEvent();
		}, this) );

		this.elems.userListBtn.click( $.proxy(function(e) {
			this.onClickUserList.dispatchEvent();
		}, this) );

		this.model.onChange.addListener(this.updateTurn, this);

		this.model.timeRemains.addListener(this.updateTurnTimer, this);

		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},

	turnStatusElem: function() {
		var lastTurn = this.model.game.lastTurn();
		var resultValues = this.model.turnResultValues;
		var player = this.model.player;
		var turnStatus;
		if ( !this.model.game.isOver() ) {
			turnStatus = this.model.game.myTurn(player)
				? this.elems.statusYourTurn
				: this.elems.statusOpponentTurn;
		}
		else {
			if (lastTurn.result == resultValues.DRAW)
				turnStatus = this.elems.statusDraw;
			else if (lastTurn.result == resultValues.WIN)
				turnStatus = (lastTurn.playerId == player.id)
					? this.elems.statusYouWin
					: this.elems.statusOpponentWin;
			else if (lastTurn.result == resultValues.SURRENDER)
				turnStatus = (lastTurn.playerId == player.id)
					? this.elems.statusYouSurrender
					: this.elems.statusOpponentSurrender;
			else if (lastTurn.result == resultValues.TIMEOUT)
				turnStatus = (lastTurn.playerId == player.id)
					? this.elems.statusYouTimeout
					: this.elems.statusOpponentTimeout;
		}
		return turnStatus;
	},
	
	clearCells: function(){
		var contentNoneHtml = this.cellContentHtml(this.model.cellContentValues.CONTENT_NONE);
		for (var i=0; i<3; i++)
			for (var j=0; j<3; j++)
				this.elems.gameField.find('#cell' +i +j).html(contentNoneHtml);
	},
	
	displayCell: function(cell) {
		if (cell)
			this.elems.gameField.find('#cell' +cell.col +cell.row)
				.hide().html(this.cellContentHtml(cell.content)).show('fast');
	},
	
	cellContentHtml: function(content) {
		var content;
		switch (parseInt(content)) {
			case this.model.cellContentValues.CONTENT_NONE:
				content = '&nbsp;';
				break;
			case this.model.cellContentValues.CONTENT_X:
				content = '&#215;';
				break;
			case this.model.cellContentValues.CONTENT_0:
				content = '&#927;';
				break;
		}
		return content;
	},

	updateTurnTimer: function() {
		this.elems.turnTimer.text(this.model.timeRemains.value);
	},

	updateTurn: function() {
		//console.log("updateTurn",this.model.game);
		this.jqElem.find('.gameStatus').addClass('hidden');
		this.turnStatusElem().removeClass('hidden');
		var isOver = this.model.game.isOver();
		this.elems.gameOver.setDisplay(isOver);
		this.elems.surrenderBtn.setDisplay(!isOver);
		this.elems.inviteReplayBtn.setDisplay( this.model.game.proposeReplay(this.model.player) );

		var lastTurn = this.model.game.lastTurn();
		if (lastTurn)
			this.displayCell(lastTurn.cell);
	},

	update: function() {
		this.elems.opponentName.text(this.model.opponentUser.name);
		this.clearCells();
		for (var i=0; i < this.model.game.cells.items.length; i++) {
			this.displayCell(this.model.game.cells.items[i]);
		}
		this.updateTurn();
	},
	
	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}
		this.update();

		this.jqElem.removeClass('hidden');
	},
	
	hide: function() {
		this.jqElem
			? this.jqElem.addClass('hidden')
			: this.displayElem.showAfterLoad = 0;
	},

};


function GameController(model, view) {

	this.model = model;
	this.view = view;

	this.view.onClickGameField.addListener( function(sender,args){
		//console.log("onClickGameField",this.model.game);
		if ( !this.model.game.myTurn(this.model.player)
			|| !this.model.game.cells.cellIsFree(args.col,args.row) )
			return;
		this.view.gameControls.disable();
		// обновить cell - после ответа сервера
		this.doTurn(args.col,args.row);
	},this);

	this.view.onClickSurrender.addListener( function(){
		this.view.gameControls.disable();
		this.opponentCheckRequest.stop();
		this.doTurn(0,0,1);
	}, this);

	this.view.onClickUserList.addListener( function(){
		this.deactivate();
		Request.post({ action: 'me_available' });
		this.callback.call(this.context, this, { 'usersAvailable': 1 });
	}, this);

	this.view.onClickInviteReplay.addListener( function(){
		this.deactivate();
		Request.post({ action: 'invite_send', user_id: this.model.opponentUser.id, replay: 1 },function(data){

			AppWindow.get('InviteSendWindow',this.inviteCallback,this)
				.controller.setData(
					data.invite_id,
					this.model.opponentUser.name,
					1
				).invoke();
		},this,
		function(errors){
			this.displayErrors(errors);
			this.activate();
		});
	}, this);

	this.opponentCheckRequest = new RepeativeRequest.__construct(
		{ action: 'game_check_opponent' },
		Config.Timers.GAME_CHECK_OPPONENT,
		{ delayExecution: true },// stopOnError: true },
		function(data){
			if (data.last_turn) {
				this.opponentCheckRequest.stop();
				this.nextTurn(data);
			}
		},this,
		function(errors){
			this.displayErrors(errors);
		}
	);

	this.inviteCheckRequest = new RepeativeRequest.__construct(
		{ action: 'invite_incoming' },
		Config.Timers.INVITE_CHECK_INCOMING,
		{ delayExecution: true },// stopOnError: true },
		function(data){
			if (data.invites[0]) {
				this.deactivate();
				AppWindow.get('InviteWindow',this.inviteCallback,this)
					.controller.setData(data.invites[0].id, data.invites[0].userSrc.name)
					.invoke();
			}
		},this,
		function(errors){
			this.displayErrors(errors);
		}
	);	

}

GameController.prototype = {

	doTurn: function(col,row,surrender) {
		if (!surrender)
			surrender = 0;
		Request.post({
			action: 'game_do_turn', col: col, row: row,
			player_id: this.model.player.id, surrender: surrender
		}, function(data){
			this.nextTurn(data);
		},this,
		function(errors){
			this.displayErrors(errors);
			this.activate();
		});
	},
	
	nextTurn: function(data){
		var game = this.model.game;
		if (data) {
			var turn = (new Turn).deserialize(data.last_turn);
			if (data.last_turn.cell)
				turn.cell = (new Cell).deserialize(data.last_turn.cell);
			game.turns.add(turn);
			game.status = parseInt(data.game_status);
		}
		//console.log("nextTurn",game);
		this.model.setLastTurnClientTime();
		this.model.onChange.dispatchEvent();

		if (game.isOver()) {
			this.view.gameOverControls.enable();
			Request.post({ action: 'me_not_busy' });
			if ( game.expectInvite(this.model.player) )
				this.inviteCheckRequest.start();
		}
		else {
			this.view.gameControls.enable();
			if ( game.expectOpponentTurn(this.model.player) )
				this.opponentCheckRequest.start();
		}
	},

	inviteCallback: function(sender,args){
		sender.close();
		if ('inviteReject' in args) {
			this.activate();
			if (this.model.game.expectInvite(this.model.player))
				this.inviteCheckRequest.start();
		}
		else if ('game' in args) {
			this.setData(args.game, args.player, args.opponentUser)
				.invoke();
		}
		else {
			//console.log("GameWindow.inviteCallback: bad args");
		}
	},

	timedActivity: function() {
		var remains =
			Config.TURN_TIMEOUT
			+ this.model.lastTurnClientTime
			- parseInt( (new Date).getTime() / 1000 );

		if (remains < -1) {
			this.stopTurnTimer();
			if (this.model.game.myTurn(this.model.player))
				this.doTurn(0,0,1);
			else
				this.opponentCheckRequest.stop();
		}
		if (this.model.game.isOver())
			this.model.timeRemains.set(0);
		else if (remains >= 0)
			this.model.timeRemains.set(remains);
	},
	
	startTurnTimer: function() {
		this.timedActivity();
		this.turnTimer = setInterval( $.proxy(this.timedActivity,this), 1000);
	},

	stopTurnTimer: function(){
		clearInterval(this.turnTimer);
	},

	displayErrors: function(errors) {
		var parentId = this.view.displayElem.options.id;
		AppWindow.get('ErrorWindow',{ parentId: parentId, appId: parentId })
			.controller.setData(errors).invoke();
	},
	
	setData: function(game,player,opponentUser) {
		//console.log("setData",game,player,opponentUser);
		this.model.game = game;
		this.model.player = player;
		this.model.opponentUser = opponentUser;
		this.model.setLastTurnClientTime();
		return this;
	},
	
	activate: function() {
		
		this.opponentCheckRequest.data.player_id = this.model.player.id;

		this.view.gameControls.enable();
		this.view.gameOverControls.enable();
		this.startTurnTimer();
		
		this.nextTurn();
		return this;
	},
	
	deactivate: function() {
		this.view.gameControls.disable();
		this.view.gameOverControls.disable();

		this.opponentCheckRequest.stop();
		this.inviteCheckRequest.stop();
		this.stopTurnTimer();
		return this;
	}
};

function GameWindow(displayElem) {

	this.model = new GameModel;
	this.view = new GameView(this.model, displayElem);
	this.controller = new GameController(this.model, this.view);

}
GameWindow.prototype = {

}

