
function InviteModel() {
	
	this.inviteId;
	this.userName;
}

function InviteView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;

	this.controls = new View.DispatcherGroup([
		this.onClickAccept = new EventDispatcher,//({ autoDisable: 1 });
		this.onClickReject = new EventDispatcher//({ autoDisable: 1 });
	]);
	
	displayElem.load(this.resourceLoaded, this);
}

InviteView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;

		this.elems = {
			acceptBtn:	jqElem.find('#invite_accept_ctrl'),
			rejectBtn:	jqElem.find('#invite_reject_ctrl'),
			closeBtn:	jqElem.find('#invite_close_ctrl'),
		};

		this.elems.acceptBtn.click( $.proxy(function(e) {
			this.onClickAccept.dispatchEvent();
			return false;
		}, this) );

		this.elems.rejectBtn.click( $.proxy(function(e) {
			this.onClickReject.dispatchEvent();
			return false;
		}, this) );

		this.elems.closeBtn.click( $.proxy(function(e) {
			this.onClickReject.dispatchEvent();
			return false;
		}, this) );

		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},
		
	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}
		this.jqElem.find('#invite_user_name').text(this.model.userName);
		
		ModalBSManager.show(this.jqElem);
		//this.jqElem.modal({ backdrop: 'static', keyboard: false });
		return this;
	},

	hide: function() {
		this.jqElem
			? ModalBSManager.hide(this.jqElem)//this.jqElem.modal('hide')
			: this.displayElem.showAfterLoad = 0;
		return this;
	}
};

function InviteController(model, view) {

	this.model = model;
	this.view = view;

	view.onClickAccept.addListener(function(){
		this.doAccept();
	}, this);

	view.onClickReject.addListener(function(){
		this.doReject();
	}, this);
	
	this.checkInviteRequest = new RepeativeRequest.__construct(
		{ action: 'invite_incoming' },
		Config.Timers.INVITE_CHECK_INCOMING,
		{ stopOnError: true, delayExecution: true },
		function(data) {
			if (data.invites[0] && this.model.inviteId == data.invites[0].id) {
			}
			else {
				this.deactivate();
				this.callback.call(this.context,this, { inviteReject: 1 });
			}
		},this,
		function(errors) {
			this.displayErrors(errors);
		}
	);

}

InviteController.prototype = {

	doAccept: function() {
		this.deactivate();
		Request.post({ action: 'invite_accept', invite_id: this.model.inviteId },function(data){

			this.callback.call(this.context,this, {
				game:		(new Game).deserialize(data.game),
				player:		(new Player).deserialize(data.player),
				opponentUser: (new User).deserialize(data.opponent_user)
			});

		},this,
		function(errors){
			this.displayErrors(errors);
			this.activate();
		});
	},
	
	doReject: function() {
		this.deactivate();
		Request.post({ action: 'invite_reject', invite_id: this.model.inviteId },function(data){
			this.callback.call(this.context,this, { inviteReject: 1 });
		},this,
		function(errors){
			this.displayErrors(errors);
			this.activate();
		});
	},
	
	setData: function(inviteId, userName, ifReplay) {
		this.model.inviteId = inviteId;
		this.model.userName = userName;
		return this;
	},

	displayErrors: function(errors) {
		var parentId = this.view.displayElem.options.id;
		AppWindow.get('ErrorWindow',{ parentId: 'error_window_prepend', appId: parentId })
			.controller.setData(errors).invoke();
	},
	
	activate: function() {
		this.view.controls.enable();
		this.checkInviteRequest.start();
		return this;
	},
	deactivate: function() {
		this.view.controls.disable();
		this.checkInviteRequest.stop();
		return this;
	}
};

function InviteWindow(displayElem) {

	this.model = new InviteModel;
	this.view = new InviteView(this.model, displayElem);
	this.controller = new InviteController(this.model, this.view);

}
InviteWindow.prototype = {
};
