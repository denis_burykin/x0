
var AppWindow = (function(){

	function AppWindowStorage() {
	}

	AppWindowStorage.prototype = {

		defaultOptions: {
			parentId:	'main_window',
			jqAddFn:	'after'
		},

		appWindowData: {
			MainWindow: {
				resourceUrl:	'html/MainWindow.html',
				options:		{ id:'main_window', parentId:'append_main_window', jqAddFn:'append' },
				//codeUrl:		'js/MainWindow.js',
				construct:		'MainWindow'
			},
			ErrorWindow: {
				resourceUrl:	'html/ErrorWindow.html',
				options:		{ id:'error_window', jqAddFn:'prepend' },
				construct:		'ErrorWindow'
			},
			LoginWindow: {
				resourceUrl:	'html/LoginWindow.html',
				options:		{ id:'login_window' },
				construct:		'LoginWindow'
			},
			RegistrationWindow: {
				resourceUrl:	'html/RegistrationWindow.html',
				options:		{ id:'registration_window' },
				construct:		'RegistrationWindow',
			},
			UsersAvailableWindow: {
				resourceUrl:	'html/UsersAvailableWindow.html',
				options:		{ id:'users_available_window' },
				construct:		'UsersAvailableWindow'
			},
			InviteSendWindow: {
				resourceUrl:	'html/InviteSendWindow.html',
				options:		{ id:'invite_send_window' },
				construct:		'InviteSendWindow'
			},
			InviteWindow: {
				resourceUrl:	'html/InviteWindow.html',
				options:		{ id:'invite_window' },
				construct:		'InviteWindow'
			},
			GameWindow: {
				resourceUrl:	'html/GameWindow.html',
				options:		{ id:'game_window' },
				construct:		'GameWindow'
			},
			LanguageSelectorWindow: {
				resourceUrl:	'html/LanguageSelectorWindow.html',
				options:		{ id:'language_selector_window', parentId:'language_selector_window_after' },
				construct:		'LanguageSelectorWindow'
			}
			
		},
		
		get: function(name,appId){
			if (!this.appWindowData[name]) {
				console.log("AppWindowStorage.get(" +name +"): no such name");
				return;
			}
			return this[name +appId];
		},
		
		set: function(name,appId,obj){
			this[name +appId] = obj;
		},
		remove: function(name,appId){
			delete this[name +appId];
		},

	};

	var appWindowStorage = new AppWindowStorage;


	var controllerFunctions = {

		invoke: function() {
			return this.activate().view.show();
		},
		close: function() {
			return this.deactivate().view.hide();
		},
	};

	//	окно SomeWindow
	//	экземпляр someWindow
	//	someWindow. {model view controller}

	/*
	*	Возвращает окно (при необходимости создает)
	*	Список окон и их свойств в AppWindowStorage
	*
	*	Используется DisplayElement, который подгружает html,
	*	вставляет в DOM и возвращает jq объект.
	*/
	function get(name,callback,context,options) {

		if (!$.isFunction(callback)) {
			options = callback;
			callback = null;
		}
		if (!options)
			options = {};
		if (!('appId' in options))
			options.appId  = '';

		var appWindow = appWindowStorage.get(name,options.appId);
		//	нет проверки на второй вызов окна с одинаковым appId
		//	при незакрытом первом
		if (appWindow) {
			if (callback)
				setCallback(appWindow,callback,context);
			return appWindow;
		}

		var appWindowData = appWindowStorage.appWindowData[name];
		for (var k in appWindowData.options) {
			if (!(k in options))
				options[k] = appWindowData.options[k];
		}
		for (var k in appWindowStorage.defaultOptions) {
			if (!(k in options))
				options[k] = appWindowStorage.defaultOptions[k];
		}

		var displayElement = new View.DisplayElement(appWindowData.resourceUrl,options);
		appWindow = new window[ appWindowData.construct ](displayElement);
		if (!appWindow) {
			console.log("AppWindow.get: new window[" +name +"]."
				+appWindowData.construct +" : no object created");
			return;
		}

		for (var fn in controllerFunctions)
			appWindow.controller[fn] = controllerFunctions[fn];
		appWindow.controller.appId = options.appId;
		appWindow.view.displayElem = displayElement;

		appWindowStorage.set(name,options.appId,appWindow);
		if (callback)
			setCallback(appWindow,callback,context);
		return appWindow;


		function setCallback(fn,callback,context){
			fn.controller.callback = callback;
			fn.controller.context = context;
		}

	}

	return {
		get:	get
	};

})(); // AppWindow
