
function LoginModel() {
}

function LoginView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;

	this.controls = new View.DispatcherGroup([
		this.onClickLogin = new EventDispatcher,
		this.onClickRegistration = new EventDispatcher
	]);//, { autoDisable: 1 });
	
	displayElem.load(this.resourceLoaded, this);
}

LoginView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;

		this.elems = {
			loginBtn:		$('#login_ctrl'),
			registrationLink:$('#user_reg_new_window_ctrl')
		};

		this.elems.loginBtn.click( $.proxy(function(e) {
			this.onClickLogin.dispatchEvent({
				formData:	$("#form_login").serializeArray()
			});
			return false;
		}, this) );

		this.elems.registrationLink.click( $.proxy(function(e) {
			this.onClickRegistration.dispatchEvent();
			return false;
		}, this) );

		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},
		
	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}

		this.jqElem.removeClass('hidden');
		return this;
	},
	
	hide: function() {
		this.jqElem
			? this.jqElem.addClass('hidden')
			: this.displayElem.showAfterLoad = 0;
		return this;
	}
};

function LoginController(model, view) {

	this.model = model;
	this.view = view;

	view.onClickLogin.addListener(this.clickLogin, this);

	view.onClickRegistration.addListener(function(){
		this.callback.call(this.context,this, { clickRegistration: 1 });
	}, this);
}

LoginController.prototype = {

	clickLogin: function(sender,args){
		var formData = args.formData;
		formData.push({ name: 'action', value: 'login' });
		Request.post(formData, function(data){
			this.callback.call(this.context,this, { loginSuccess: 1 });
		},
		this, function(errors){
			var parentId = this.view.displayElem.options.id;
			AppWindow.get('ErrorWindow',{ parentId: parentId, appId: parentId })
				.controller.setData(errors).invoke();
			this.activate();
		});
	},
	
	invoke: function() {
		return this.activate().view.show();
	},
	close: function() {
		return this.deactivate().view.hide();
	},
	
	activate: function() {
		this.view.controls.enable();
		return this;
	},
	deactivate: function() {
		this.view.controls.disable();
		return this;
	}
};

function LoginWindow(displayElem) {

	this.model = new LoginModel;
	this.view = new LoginView(this.model, displayElem);
	this.controller = new LoginController(this.model, this.view);

}
LoginWindow.prototype = {
}
