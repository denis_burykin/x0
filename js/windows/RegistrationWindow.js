
function RegistrationModel() {
}

function RegistrationView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;
	
	this.controls = new View.DispatcherGroup([
		this.onClickRegistration = new EventDispatcher,
		this.onClickLogin = new EventDispatcher
	], { autoDisable: 1 });
	
	displayElem.load(this.resourceLoaded, this);
}

RegistrationView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;

		this.elems = {
			registrationBtn:$('#registration_ctrl'),
			loginLink:		$('#login_ctrl')
		};

		this.elems.registrationBtn.click( $.proxy(function(e) {
			this.onClickRegistration.dispatchEvent({
				formData:	$("#form_registration").serializeArray()
			});
			return false;
		}, this) );

		this.elems.loginLink.click( $.proxy(function(e) {
			this.onClickLogin.dispatchEvent();
			return false;
		}, this) );

		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},
		
	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}

		this.jqElem.removeClass('hidden');
		return this;
	},

	hide: function() {
		this.jqElem
			? this.jqElem.addClass('hidden')
			: this.displayElem.showAfterLoad = 0;
		return this;
	}
};

function RegistrationController(model, view) {

	this.model = model;
	this.view = view;

	view.onClickLogin.addListener(function(){
		this.callback.call(this.context,this, { clickLogin: 1 });
	}, this);

	view.onClickRegistration.addListener(this.clickRegistration, this);

}

RegistrationController.prototype = {

	clickRegistration: function(sender,args){
		var formData = args.formData;
		formData.push({ name: 'action', value: 'registration' });
		Request.post(formData, function(data){
			this.callback.call(this.context,this, { loginSuccess: 1 });
		},
		this, function(errors){
			var parentId = this.view.displayElem.options.id;
			AppWindow.get('ErrorWindow',{ parentId: parentId, appId: parentId })
				.controller.setData(errors).invoke();
			this.activate();
		});
	},

	invoke: function() {
		return this.activate().view.show();
	},
	close: function() {
		return this.deactivate().view.hide();
	},
	
	activate: function() {
		this.view.controls.enable();
		return this;
	},
	deactivate: function() {
		this.view.controls.disable();
		return this;
	}

};

function RegistrationWindow(displayElem) {

	this.model = new RegistrationModel;
	this.view = new RegistrationView(this.model, displayElem);
	this.controller = new RegistrationController(this.model, this.view);

}
