
function ErrorModel() {
	this.errors = new EventDispatcher({ value: null });
}

function ErrorView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;

	this.onClickClose = new EventDispatcher;//({ autoDisable: 1 });

	displayElem.load(this.resourceLoaded, this);
}

ErrorView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;
		
		this.elems = {
			closeBtn:	jqElem.find('#close_ctrl'),
			errorMsg:	jqElem.find('#error_msg')
		};

		this.elems.closeBtn.click( $.proxy(function(e) {
			this.onClickClose.dispatchEvent({});
			return false;
		}, this) );

		this.model.errors.addListener(this.update, this);

		// по умолчанию !showAfterLoad
		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},

	update: function() {
		var content = '';
		var errors = this.model.errors.value;
		if (errors) {
			for (var i=0; i < errors.length; i++) {
				content += '<font id="' +errors[i].id +'" class="lang"></font>';
				content += ' ' +errors[i].message +"<br>\n";
			}
		}
		this.elems.errorMsg.html(content);
		if (errors)
			View.updateTextElements(this.elems.errorMsg);
	},
	
	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}
		this.update();
		
		this.jqElem.removeClass('hidden')
		return this;
	},
	
	hide: function() {
		this.jqElem
			? this.jqElem.addClass('hidden')
			: this.displayElem.showAfterLoad = 0;
		return this;
	}
};

function ErrorController(model, view) {

	this.model = model;
	this.view = view;

	view.onClickClose.addListener(function(){
		// остановить обработку ввода пользователя и периодические запросы к серверу
		this.close();
		if (this.callback)
			this.callback.call(this.context, this);		
	},this);

}

ErrorController.prototype = {

	setData: function(errors) {
		
		this.model.errors.set(errors);
		return this;
	},
	/*
	*	invoke()
	*	close()
	*
	*	activate() вызывается как при 1-м показе, так и при reuse окна
	*/
	activate: function() {
		return this;
	},	
	deactivate: function() {
		return this;
	}
};

function ErrorWindow(displayElement) {

	this.model = new ErrorModel;
	this.view = new ErrorView(this.model, displayElement);
	this.controller = new ErrorController(this.model, this.view);

}
