
function LanguageSelectorModel() {

	this.languages = (new LanguageCollection)
		.deserialize(ModelDefinition.Language.data);

	this.languageSelectedId = new EventDispatcher({ value: Config.LANG_ID_DEFAULT });
}

function LanguageSelectorView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;

	this.onChangeDropDown = new EventDispatcher;

	displayElem.load(this.resourceLoaded, this);
}

LanguageSelectorView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = this.displayElem.jqElem;
		
		this.elems = {
			languageSelected:	jqElem.find('#languageSelected'),
			dropDown:			jqElem.find('#dropDown')
		};

		this.elems.dropDown.click( $.proxy(function(e) {
			//console.log("dropDown.change",e.target);
			if ( (res = e.target.id.match(/^language(\d+)$/)) )
				this.onChangeDropDown.dispatchEvent({ id: res[1] });
			return true;
		}, this) );

		this.model.languageSelectedId.addListener(this.updateSelected, this);

		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},

	updateSelected: function(){
		this.elems.languageSelected.html(
			this.model.languages.getById(this.model.languageSelectedId.value).name
		);
	},

	updateList: function() {
		var content = '';
		for (var i=0; i < this.model.languages.items.length; i++) {
			var language = this.model.languages.items[i];
			content += '<li><a href="#" id="language' +language.id +'">';
			content += language.name + "</a></li>\n";
		}
		this.elems.dropDown.html(content);
	},

	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}

		this.updateSelected();
		this.updateList();
		
		this.jqElem.removeClass('hidden');
		return this;
	},
	
	hide: function() {
		this.jqElem
			? this.jqElem.addClass('hidden')
			: this.displayElem.showAfterLoad = 0;
		return this;
	}
};

function LanguageSelectorController(model, view) {

	this.model = model;
	this.view = view;

	view.onChangeDropDown.addListener(function(sender,args){
		this.model.languageSelectedId.set(args.id);

		TextStorage.selectLanguage(args.id, function() {
			View.updateTextElements();
		});
		
	},this);

}

LanguageSelectorController.prototype = {

	activate: function() {
		return this;
	},	
	deactivate: function() {
		return this;
	}
};

function LanguageSelectorWindow(displayElement) {

	this.model = new LanguageSelectorModel;
	this.view = new LanguageSelectorView(this.model, displayElement);
	this.controller = new LanguageSelectorController(this.model, this.view);

}
