
function MainModel() {

	this.loggedIn = new EventDispatcher({ value: 0 });
	this.userName = new EventDispatcher({ value: '' });

}

MainModel.prototype = {

};


function MainView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElem;

	this.onClickLogout = new EventDispatcher({ autoDisable: 1 });

	displayElem.load(this.resourceLoaded,this);
}

MainView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;
		
		this.elems = {
			logoutBtn:		jqElem.find('#logout_ctrl'),
			navbarUserName:	jqElem.find('#navbar_user_name')
		};
		
		this.elems.logoutBtn.click( $.proxy(function(e) {
			//console.log(e,this);
			this.onClickLogout.dispatchEvent();
			return false;
		}, this) );


		this.model.loggedIn.addListener(this.updateLoggedIn, this)
			.dispatchEvent();

		this.model.userName.addListener( function(userName) {
			this.elems.navbarUserName.text(userName.value);
		},this)
			.dispatchEvent();

		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},

	updateLoggedIn: function(loggedIn) {
		if (loggedIn.value) {
			this.elems.logoutBtn.removeClass('hidden');
		}
		else
			this.elems.logoutBtn.addClass('hidden');
	},

	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}
		
		this.jqElem.removeClass('hidden');
		return this;
	},
	
	hide: function() {
		this.jqElem
			? this.jqElem.addClass('hidden')
			: this.displayElem.showAfterLoad = 0;
		return this;
	}

}

// Начало работы приложения
// подгружены все js файлы
//
function MainController(model, view) {

	this.model = model;
	this.view = view;
	
	//this.model.loggedIn.set(0);
	view.onClickLogout.addListener(function(){
		this.doLogout();
	}, this);

	AppWindow.get('LanguageSelectorWindow').controller.invoke();

	this.doLoadUser();

}


MainController.prototype = {

	doLogout: function(){
		RepeativeRequest.stopAll();

		Request.post({ action: 'logout' }, function(data){
			this.model.loggedIn.set(0);
			this.model.userName.set('');
			/*
			*	Менеджера окон, хранящего все открытые окна - нет.
			*	При увеличении размера проекта - обязательно.
			*	Пока hack.
			*/
			$('.gameWindow').addClass('hidden');
			AppWindow.get('LoginWindow',this.mainCallback,this)
				.controller.invoke();
		},this,
		function(errors){
			//	возможно пользователь залогинился в другом окне
			/*if (errors[0].id != 'err_auth_required'
				&& errors[0].id != 'err_session_timeout'
			) {
				this.displayErrors(errors);
			}*/
			$('.gameWindow').addClass('hidden');
			AppWindow.get('LoginWindow',this.mainCallback,this)
				.controller.invoke();
		});
	},

	doLoadUser: function() {
		Request.post({ action: 'load_user' }, function(data){
			this.model.loggedIn.set(1);
			this.model.userName.set(data.user.name);
			this.view.onClickLogout.enable();
			
			if ('game' in data)
				AppWindow.get('GameWindow',this.mainCallback,this)
				.controller.setData(
					(new Game).deserialize(data.game),
					(new Player).deserialize(data.player),
					(new User).deserialize(data.opponent_user)
				).invoke();

			else if ('sent_invites' in data)
				AppWindow.get('InviteSendWindow',this.mainCallback,this)
					.controller.setData(
						data.sent_invites[0].id,
						data.sent_invite_dst_user.name
					).invoke();

			else
				AppWindow.get('UsersAvailableWindow',this.mainCallback,this)
					.controller.invoke();

		},this,
		function(errors) {
			//this.displayErrors(errors);
			//console.log('load_user',errors);
			AppWindow.get('LoginWindow',this.mainCallback,this)
				.controller.invoke();
		});
	},


	mainCallback: function(sender,args){
		if (!args)
			args = {};
		sender.close();
		if ('loginSuccess' in args) {
			this.doLoadUser();
		}
		else if ('clickLogin' in args) {
			AppWindow.get('LoginWindow',this.mainCallback,this)
				.controller.invoke();
		}
		else if ('clickRegistration' in args) {
			AppWindow.get('RegistrationWindow',this.mainCallback,this)
				.controller.invoke();
		}
		else if ('game' in args) {
			AppWindow.get('GameWindow',this.mainCallback,this)
				.controller.setData(args.game, args.player, args.opponentUser)
				.invoke();
		}
		else {//if ('usersAvailable' in args) {
			AppWindow.get('UsersAvailableWindow',this.mainCallback,this)
				.controller.invoke();
		}/*
		else {
			//console.log("MainWindow.mainCallback: bad args",sender,args);
		}*/
	},

	displayErrors: function(errors) {
		var parentId = this.view.displayElem.options.id;
		AppWindow.get('ErrorWindow',{ parentId: parentId, jqAddFn:'after', appId: parentId })
			.controller.setData(errors).invoke();
	},
	
	activate: function() {
		this.view.onClickLogout.enable();
		return this;
	},
	deactivate: function() {
		return this;
	}
};

function MainWindow(displayElem) {

	this.model = new MainModel;
	this.view = new MainView(this.model, displayElem);
	this.controller = new MainController(this.model, this.view);

}
MainWindow.prototype = {
}
