
function SomeModel() {
}

function SomeView(model, displayElem) {

	this.model = model;
	this.displayElem = displayElemж

	// Здесь то, для чего не нужен html-компонент
	// Интерфейс контроллеру
	this.onClickZZZ = new EventDispatcher({ autoDisable: 1 });

	displayElem.load(this.resourceLoaded, this);
}

SomeView.prototype = {

	resourceLoaded: function(jqElem) {
		this.jqElem = jqElem;
		
		this.elems = {
			loginBtn:	jqElem.find('#login_ctrl'),
		};

		this.elems.loginBtn.click( $.proxy(function(e) {
			this.onClickLogin.dispatchEvent({		});
			return false;
		}, this) );

		/*
		*
		*
		*/

		this.displayElem.showAfterLoad ? this.show() : this.hide();
	},
		
	show: function() {
		if (!this.jqElem) {
			this.displayElem.showAfterLoad = 1;
			return;
		}
		//console.log(this);
		
		this.jqElem.removeClass('hidden')
		return this;
	},
	
	hide: function() {
		this.jqElem
			? this.jqElem.addClass('hidden')
			: this.displayElem.showAfterLoad = 0;
		return this;
	}
};

function SomeController(model, view) {

	this.model = model;
	this.view = view;

	view.onClickLogin.addListener(this.clickLogin, this);

	view.onClickSomeBtn.addListener(function(){
		// остановить обработку ввода пользователя и периодические запросы к серверу
		this.deactivate();
		
		// показ окна. Окно если что будет создано.
		// Можно без new
		var w = AppWindow.get('RegistrationWindow',this.usersAvailableCallback,this)
			.controller.invoke();
	},this);

}

SomeController.prototype = {

	clickLogin: function(sender,args){
		var formData = args.formData;
		formData.push({ name: 'action', value: 'login' });
		Request.post(formData, function(data){
			this.onLoginSuccess.dispatchEvent();
		},
		this, function(errors){
			alert("login error: "+errors);
			this.view.show();
		});
	},

	/*
	*	invoke()
	*	close()
	*
	*	activate() вызывается как при 1-м показе, так и при reuse окна
	*/
	activate: function() {
		return this;
	},	
	deactivate: function() {
		return this;
	}
};

function SomeWindow(displayElement) {

	this.model = new SomeModel;
	this.view = new SomeView(this.model, displayElement);
	this.controller = new SomeController(this.model, this.view);

}
