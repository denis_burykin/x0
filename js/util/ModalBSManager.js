
var ModalBSManager = (function(){

	var queue = [];
	
	function show(jqElem){
		queue.push({ jqElem: jqElem, status: 'queue' });
		if (queue.length == 1)
			doShow();
	}

	function doShow() {
		queue[0].status = 'show';
		queue[0].jqElem.one('hidden.bs.modal',onHidden);
		queue[0].jqElem.modal({ backdrop: 'static', keyboard: false });
	}
	
	function hide(jqElem){
		if (!queue.length) {
			console.log("ModalBSManager.hide: queue is empty");
			return;
		}
		if (queue[0].jqElem === jqElem) {
			if (queue[0].status == 'show') {
				queue[0].status = 'hide';
				jqElem.modal('hide');
			}
			return;
		}
		for (var i=1; i < queue.length; i++)
			if (queue[i].jqElem === jqElem) {
				console.log("ModalBSManager.hide: index=" +i);
				queue.splice(i, 1);
				break;
			}
	}
	
	function onHidden(e){
		if (e.target.id == queue[0].jqElem.attr('id')) {
			queue.pop();
			if (queue.length)
				doShow();
		} else {
			console.log("ModalBSManager.onHidden",e.target.id,queue[0].jqElem.attr('id'));
		}
	}

	return {
		show:	show,
		hide:	hide
	};

})();
