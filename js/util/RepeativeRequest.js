
var RepeativeRequest = (function(){

	var id = 1;
	var requests = {};

	function stopAll() {
		for (var i in requests)
			if (requests[i].running)
				requests[i].stop();
	}

	var __construct = function(data,interval,options,callback,context,errorCallback) {
		this.data = data;
		this.interval = interval;
		if (!interval) {
			this.interval = 10000;
			console.log("RepeativeRequest: zero interval", data.action);
		}
		this.options = options ? options : {};
		this.callback = callback;
		this.context = context;
		this.errorCallback = errorCallback;
		this.timer = false;
		this.running = false; // периодически выполняются запросы
		this.requestGoing = false; // происходит запрос к серверу
		this.id = id;
		requests[id++] = this;
	}

	__construct.prototype = {
		requestCallback: function(data) {
			this.requestGoing = false;
			if (!this.running) return;
			this.runRequest();
			if (this.callback)
				this.context ? this.callback.call(this.context,data)
							: this.callback(data);
		},
		requestErrorCallback: function(errors) {
			this.requestGoing = false;
			if (!this.running) return;
			if ( 'stopOnError' in this.options
				&& this.options['stopOnError']
			) {
				this.stop();
			}
			else
				this.runRequest();
			if (this.errorCallback)
				this.context ? this.errorCallback.call(this.context,errors)
							: this.errorCallback(errors);
		},
		doRequest: function() {
			//alert('doRequest: '+this.formData);
			this.requestGoing = true;
			Request.post(this.data,this.requestCallback,this,this.requestErrorCallback);
		},
		runRequest: function() {
			this.timer = setTimeout( $.proxy(this.doRequest, this), this.interval);
		},
		start: function() {
			this.running = true;
			if ( !('delayExecution' in this.options)
				|| !this.options['delayExecution']
			)
				this.doRequest();
			else
				this.runRequest();
		},
		stop: function() {
			clearTimeout(this.timer);
			this.running = false;
		}
	};
	return {
		__construct:	__construct,
		stopAll:		stopAll
	};

})(); // RepeativeRequest
