
$.prototype.setDisplay = function(ifDisplay) {
	ifDisplay
		? this.removeClass('hidden')
		: this.addClass('hidden');
	return this;
}

var View = (function(){

	function updateTextElements(jqParent) {
		var elems = jqParent ? jqParent.find('.lang') : $('.lang');
		elems.filter('font').each(function(){
			$(this).text(TextStorage.getText(this.id));
		});
		//elems.filter(':button').each(function(){
		//	$(this).val(TextStorage.getText(this.id));
		//});
		elems.filter('input[placeholder]').each(function(){
			$(this).attr('placeholder',TextStorage.getText(this.id))
		});
	}


	function DispatcherGroup(dispatchers, options) {
		this.dispatchers = dispatchers;
		for (var i=0; i < dispatchers.length; i++)
			dispatchers[i].addListener(this.callback,this);
		this.autoDisable = options && 'autoDisable' in options
			? options.autoDisable
			: 0;
	}

	DispatcherGroup.prototype = {

		enable: function() {
			for (var i=0; i < this.dispatchers.length; i++)
				this.dispatchers[i].enable();
		},
		callback: function() {
			if (this.autoDisable)
				this.disable();
		},
		disable: function() {
			for (var i=0; i < this.dispatchers.length; i++)
				this.dispatchers[i].disable();
		}

	}; // DispatcherGroup


	/*
	*	Функционал используется в AppWindow, AppWindowStorage
	*
	*	Подгрузить html
	*	вставить/убрать в DOM
	*	вернуть jq объект
	*/
	function DisplayElement(url, options) {

		this.url = url;
		this.options = options;
		if ( !('parentId' in options && 'jqAddFn' in options 
			&& 'id' in options && 'appId' in options)
		) {
			console.log("DisplayElement(" +url +"): bad options", options);
			return;
		}
		this.showAfterLoad = 0; // показывать после загрузки?
	}

	DisplayElement.prototype = {

		load: function(callback,context) {
			this.callback = callback;
			this.context = context;
			new Resource.Load(this.url, this.requestCallback, this);
			return this;
		},
		
		requestCallback: function(resources){
			this.resource = resources[0];
			var options = this.options;
			var jqParent = $('#' +options.parentId);
			if (!jqParent) {
				alert("DisplayElement.load: no jqParent id=" +options.parentId);
				return;
			}
			jqParent[options.jqAddFn](this.resource.content);
			this.jqElem = $('#' +options.id);
			//	Здесь еще предыдущее окно могло не подгрузиться
			//	(механизма проверки нет)
			if (!this.jqElem) {
				console.log("DisplayElement.load: no jqElem id=" +options.id);
				return;
			}
			if (options.appId)
				this.jqElem.attr('id', options.id +options.appId);

			View.updateTextElements(this.jqElem);

			this.callback.call(this.context, this.jqElem);
		},
		
		remove: function(){
		},

	}; // DisplayElement

	
	return {
		updateTextElements:	updateTextElements,
		DispatcherGroup:	DispatcherGroup,
		DisplayElement:		DisplayElement
	};

})();
