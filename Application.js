
$(document).ready(function(){
	Application.init();
});


var Application = (function(){
	
	function init() {

		result = window.location.search.match(new RegExp('^\\?([a-zA-Z0-9]{16})$'));
		if (!result) {
			setTimeout(function() {
				window.location.search = '?' +generateAlphanumericRandom(16);
			}, 100);
			return;
		}
		Request.setSessionKey(result[1]);

		new Resource.Load([
			'js/Config.js',
			'js/model/ModelDefinition.js'
		], initStage2);
	}

	function initStage2() {
		new Resource.Load([
			'js/model/AbstractModel.js'
		], initStage3);
	}

	function initStage3() {
		new Resource.Load([
			'js/model/ModelBase.js',
			'js/util/View.js',
			'js/util/RepeativeRequest.js',
			'js/util/ModalBSManager.js'
		], initStage4);
	}

	function initStage4() {
		new Resource.Load([
			'js/model/Model.js',
			'js/model/Game.js',
			'js/model/CellCollection.js',
			'js/model/TextStorage.js',
			'js/windows/AppWindow.js',
			'html/ErrorWindow.html'
		], initStage5);
	}

	function initStage5() {
		TextStorage.selectLanguage(Config.LANG_ID_DEFAULT, "force");
		
		new Resource.Load([
			'js/windows/MainWindow.js',
			'js/windows/ErrorWindow.js',
			'js/windows/LanguageSelectorWindow.js',
			'js/windows/LoginWindow.js',
			'js/windows/RegistrationWindow.js',
			'js/windows/UsersAvailableWindow.js',
			'js/windows/InviteSendWindow.js',
			'js/windows/InviteWindow.js',
			'js/windows/GameWindow.js'
		], initCompleted);
	}


	var mainWindow;
	function main() {
		return mainWindow;
	}

	function initCompleted() {
		//console.log("initCompleted() started.");

		mainWindow = AppWindow.get('MainWindow');
		mainWindow.controller.invoke();

	}
	
	return {
		init:			init,
		main:			main // for debugging
	};

})(); // Application

function generateAlphanumericRandom(len) {
	var str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
	var res = '';
	for (var i=0; i < len; i++)
		res += str.substr( Math.floor(Math.random()*62), 1 );
	return res;
}

function EventDispatcher(obj) {
	if (obj)
		for (var k in obj)
			this[k] = obj[k];
	if (!('autoDisable' in this))
		this.autoDisable = 0;
	this.listeners = [];
	this.disabled = 0;
}

EventDispatcher.prototype = {

	addListener: function (func, context) {
		if (!('listeners' in this)) {
			this.listeners = [{ func: func, context: context }];
			return this;
		}
		for (var i=0; i < this.listeners.length; i++)
			if (this.listeners[i].func == func)
				return this;
		this.listeners.push({ func: func, context: context });
		return this;
	},

/*	removeListener: function (listener) {
		this.listeners = _.without(this.listeners, listener);
	},
*/
	dispatchEvent: function (args) {
		if (this.disabled || !('listeners' in this) )
			return;
		//if (!args)
		//	args = {};
		//args.eventDispatcher = this;
		if (this.autoDisable)
			this.disabled = 1;
		for (var i = 0; i < this.listeners.length; i++) {
			var listener = this.listeners[i];
			if (!listener.context)
				listener.func(this, args);
			else
				//$.proxy(listener.func, listener.context)(this, args);
				listener.func.call(listener.context, this, args);
		}
	},
	
	disable: function() {
		this.disabled = 1;
		return this;
	},
	enable: function() {
		this.disabled = 0;
		return this;
	},
	
	set: function(newValue) {
		//console.log("ED set: newValue=" +newValue,newValue,this);
		// новое значение равно предыдущему - event не вызывается
		if ('value' in this && this.value == newValue) {
			return;
		}
		var previousValue = ('value' in this) ? this.value : null;
		this.value = newValue;
		this.dispatchEvent({ previousValue: previousValue });
		return this;
	}
	
}; // EventDispatcher

var Resource = (function(){

	var resourceURL = '';
	
	var id = 1;
	var resourceById = {};
	var resourceByUrl = {};

	var STATUS_OK		= 1;
	var STATUS_LOADING	= 2;
	var STATUS_ERROR	= 3;

	function Resource(url){
		if (url in resourceByUrl)
			return resourceByUrl[url];
		this.id = id++;
		this.url = url;
		this.status = STATUS_LOADING;
		resourceById[this.id] = this;
		resourceByUrl[url] = this;
		Request.get(resourceURL + url, this.requestCallback, this,this.requestErrorCallback);
	}
	
	Resource.prototype = {
		requestCallback: function(data){
			this.status = STATUS_OK;
			this.content = data;
			loadComplete(this);
		},
		requestErrorCallback: function(errors){
			this.status = STATUS_ERROR;// ТОDO: добавить обработку
		},
	};

	// оформить надо конечно список загрузок отдельным классом
	var loads = [];
	
	function loadComplete(resource) {
		var loadsCompleted = [];
		for (var i=0; i < loads.length; i++) {
			var load = loads[i];
			if (load.hasResource(resource) && !load.hasIncomplete()) {
				loadsCompleted.push(load);
				loads.splice(i--,1);
			}
		}
		for (var i=0; i < loadsCompleted.length; i++)
			loadsCompleted[i].doCallback();
	}
	
	// загрузка (несколько ресурсов)
	function Load(urls,callback,context,errorCallback) {
		this.callback = callback;
		this.context = context;
		this.errorCallback = errorCallback;
		if (!$.isArray(urls))
			urls = [ urls ];
		var resources = [];
		$.each(urls, function(index, url) {
			if (url && url != '') {
				var resource = new Resource(url);
				resources.push(resource);
			}
		});

		this.resources = resources;
		if (!this.hasIncomplete()) {
			this.doCallback();
			return;
		}
		loads.push(this);
	}
	
	Load.prototype = {
		hasResource: function(resource) {
			for (var i=0; i < this.resources.length; i++)
				if (this.resources[i] === resource)
					return 1;
			return 0;
			//alert(this.ids);
			//return this.ids.indexOf(id) != -1;
		},
		hasIncomplete: function(){
			for (var i=0; i < this.resources.length; i++)
				if (this.resources[i].status != STATUS_OK)
					return 1;
			return 0;
		},
		doCallback: function(){
			if (!this.callback)
				return;
			this.context
				? this.callback.call(this.context, this.resources)
				: this.callback(this.resources);
		}	};
	
	return {
		Load:		Load,
		resourceById:resourceById,
		loads:loads
	};
	
})(); // Resource


var Request = (function(){

	var requestURL = 'request.php';

	function get(resource, callback, context, errorCallback) {
		$.get(resource, function(data) {
			if (callback)
				context ? callback.call(context,data) : callback(data);
		}).fail(function(jqXHR, textStatus, errorThrown){
			alert( "error: " +resource +' ' +errorThrown + ' ' + textStatus);
			errors =[{
				id:	'err_request_fail',
				text:	'Request to server failed',
				message: errorThrown + ' ' + textStatus
			}];
			if (errorCallback)
				context ? errorCallback.call(context,errors) : errorCallback(errors);
		});
	}

	var	sessionKey	= '';

	function setSessionKey(key) {
		sessionKey = key;
	}

	function post(formData, callback, context, errorCallback){
		if (sessionKey)
			$.isArray(formData)
				? formData.push({ name: 'session_key', value: sessionKey })
				: formData.session_key = sessionKey;
		//console.log(formData);
		var data,errors;
		$.post(requestURL, formData, function(responseData){
			//console.log(responseData);
			try {
				data = JSON.parse(responseData);
				if ( !('request_success' in data) || !data.request_success)
					throw true;
			} catch(e) {
				errors = [{
					id:	'err_response_invalid',
					text:	'Invalid response from server',
					message: ''
				}];
				callError();
				//if (errorCallback)
				//	context ? errorCallback.call(context,errors) : errorCallback(errors);
				return;
			}
			if ('session_key' in data)
				setSessionKey(data.session_key);
			if (data.errors.length) {
				errors = data.errors;
				callError();
			} else {
				if (callback)
					context ? callback.call(context,data) : callback(data);
			}
		}).fail(function(jqXHR, textStatus, errorThrown){
//alert(errorThrown + ':' + textStatus+'!');
			errors =[{
				id:	'err_request_fail',
				text:	'Request to server failed',
				message: errorThrown + ' ' + textStatus
			}]
			callError();
		});
		function callError() {
			if (errorCallback)
				context ? errorCallback.call(context,errors) : errorCallback(errors);
		}
	}

	return {
		get:			get,
		setSessionKey:	setSessionKey,
		post:			post
	};

})(); // Request

